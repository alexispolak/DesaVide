﻿using UnityEngine;
using System.Collections;

public class AnimacionController : MonoBehaviour
{
    private Animator Animador { set; get; }
    private MovimientoJugador MovimientoJugador { set; get; }
    private CuerpoJugador CuerpoJugador { set; get; }
    private ControlRagdoll ControlRagdoll { set; get; }
    private CapsuleCollider Collider { set; get; }

	// Use this for initialization
	void Start ()
    {
        this.Animador = this.GetComponentInChildren<Animator>();
        this.MovimientoJugador = this.GetComponent<MovimientoJugador>();
        this.CuerpoJugador = this.GetComponent<CuerpoJugador>();
        this.ControlRagdoll = this.GetComponentInChildren<ControlRagdoll>();
        this.Collider = this.GetComponent<CapsuleCollider>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        Animador.SetBool("tocaElPiso", this.CuerpoJugador.EstaTocandoElPiso);
        Animador.SetFloat("velocidadV", this.MovimientoJugador.VelocidadMovimiento.magnitude);

        if (this.MovimientoJugador.AccionSaltar)
        {
            Animador.SetBool("saltar", true);
            Invoke("PararSalto", 0.1f);
        }

        if(this.MovimientoJugador.AccionGolpeI)
        {
            Animador.SetInteger("golpe", 2);
            Invoke("PararGolpes", 0.1f);
        }
        if (this.MovimientoJugador.AccionGolpeD)
        {
            Animador.SetInteger("golpe", 1);
            Invoke("PararGolpes", 0.1f);
        }
        if (this.MovimientoJugador.AccionPatada)
        {
            Animador.SetInteger("golpe", 3);
            Invoke("PararGolpes", 0.1f);
        }
        if (this.MovimientoJugador.AccionBarrida)
        {
            Animador.SetInteger("golpe", 5);
            Invoke("PararGolpes", 0.1f);
        }
        if (this.MovimientoJugador.AccionTirar)
        {
            Animador.SetInteger("golpe", 4);
            Invoke("PararGolpes", 0.1f);
        }

        if (this.ControlRagdoll.GuardarPosicionFinalRagdoll)
        {
            Animador.SetFloat("levantarse", this.ControlRagdoll.EstaDeEspaldas);
            this.ControlRagdoll.GuardarPosicionFinalRagdoll = false;
        }

        this.DistanciaAlPiso();
    }

    private void PararSalto()
    {
        Animador.SetBool("saltar", false);
    }

    private void PararGolpes()
    {
        Animador.SetInteger("golpe", 0);
    }

    private void DistanciaAlPiso()
    {
        if (Animador.GetFloat("curvaCollider") > 0)
        {
            this.Collider.height = (1.69f - (Animador.GetFloat("curvaCollider")*1.25f));
            this.Collider.center = new Vector3(0, 0.95f - (Animador.GetFloat("curvaCollider") / 1.55f), 0);
        }
        else if (Animador.GetFloat("curvaCollider") < 0)
        {
            this.Collider.height = (1.71f + (Animador.GetFloat("curvaCollider")));
            this.Collider.center = new Vector3(0, 0.95f - (Animador.GetFloat("curvaCollider") * 1.5f), 0);
        }
    }
}
