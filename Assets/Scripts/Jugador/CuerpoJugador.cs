﻿using UnityEngine;
using System.Collections;
using System;

public class CuerpoJugador : MonoBehaviour
{
    #region ATRIBUTOS
    [SerializeField]
    private GameObject[] partesDelCuerpo;
    [SerializeField]
    private GameObject ragdoll;
    [SerializeField]
    private GameObject mesh;
    [SerializeField]
    private GameObject explosion;


    //TODO borrar estos
    public bool botonActivarRagdoll = false;

    #endregion

    #region PROPIEDADES
    //Interfaz
    private Partida Partida { set; get; }
    private Jugador Jugador { set; get; }

    //Animaciones
    private Animator ControlAnimaciones { set; get; }
    private MovimientoJugador MovimientoJugador { set; get; }


    //Componentes
    private Rigidbody RigidBody { set; get; }
    private Animator ControlAnimacionesHijo { set; get; }
    private CapsuleCollider Collider { set; get; }
    private AnimacionController ScriptControlAnimaciones { set; get; }
    private SkinnedMeshRenderer MeshRenderer { set; get; }
    public ControlRagdoll ControladorRagdoll { private set; get; }

    //Estods
    public bool EstaActivadoPersonaje { set; get; }
    public bool EstaTocandoElPiso { set; get; }
    public bool TieneQueLevantarse { set; get; }

    public GameObject Ragdoll
    {
        get
        {
            return ragdoll;
        }

        set
        {
            ragdoll = value;
        }
    }

    public GameObject Mesh
    {
        get
        {
            return mesh;
        }

        set
        {
            mesh = value;
        }
    }

    public GameObject[] PartesDelCuerpo
    {
        get
        {
            return partesDelCuerpo;
        }

        set
        {
            partesDelCuerpo = value;
        }
    }

    public GameObject Explosion
    {
        get
        {
            return explosion;
        }

        set
        {
            explosion = value;
        }
    }

    #endregion

    #region METODOS UNITY
    // Use this for initialization
    void Awake()
    {
        this.ControlAnimaciones = this.GetComponent<Animator>();
        this.MovimientoJugador = this.GetComponent<MovimientoJugador>();
 
    }

    void Start ()
    {
        this.EstaActivadoPersonaje = false;
        this.Partida = GameObject.Find("Partida").GetComponent<Partida>();
        this.Jugador = this.GetComponent<Jugador>();

        this.ControlAnimacionesHijo = this.GetComponentInChildren<Animator>();
        this.RigidBody = this.GetComponent<Rigidbody>();
        this.Collider = this.GetComponent<CapsuleCollider>();
        this.ScriptControlAnimaciones = this.GetComponent<AnimacionController>();
        this.MeshRenderer = this.Mesh.GetComponent<SkinnedMeshRenderer>();
        this.ControladorRagdoll = this.Ragdoll.GetComponent<ControlRagdoll>();
    }
	
	// Update is called once per frame
	void Update ()
    {
	    //TODO borrar esto
        if(Input.GetKey(KeyCode.Z))
        { 
            this.ControladorRagdoll.PrenderRagdoll();
            botonActivarRagdoll = false;
        }

        this.TieneQueLevantarse = false;
        this.CondicionRespawn();
    }


    #endregion

    #region METODOS
    /// <summary>
    /// Chequea si la posicion del jugador es muy inferior y hacer reaparecer al jugador
    /// </summary>
    private void CondicionRespawn()
    {
        if (this.transform.position.y < -50f)
        {
            this.Respawn();
        }
    }

    private void Respawn()
    {
        this.Partida.Murio(this.gameObject);         
    }

    /// <summary>
    /// Desactiva todos los componentes para que tome el control el ragdoll
    /// </summary>
    public void Desactivarse()
    {
        this.ControlAnimacionesHijo.enabled = false;
        this.RigidBody.isKinematic = true;
        this.Collider.enabled = false;
        this.ScriptControlAnimaciones.enabled = false;
        this.MovimientoJugador.enabled = false;
        this.MeshRenderer.enabled = false;
    }

    /// <summary>
    /// Activa todo los componentes luego que dejo de controlar el ragdoll
    /// </summary>
    public void Activarse()
    {
        this.ExplocionParticulas();

        this.TieneQueLevantarse = true;

        if (this.ControlAnimaciones != null)
            this.ControlAnimaciones.Play("Levantarse", 0);

        this.CopiarPoseRagdoll();

        this.ControlAnimacionesHijo.enabled = true;
        this.RigidBody.isKinematic = false;
        this.Collider.enabled = true;
        this.ScriptControlAnimaciones.enabled = true;
        this.MovimientoJugador.enabled = true;
        this.MeshRenderer.enabled = true;
    }

    private void ExplocionParticulas()
    {
        var posicion = this.ControladorRagdoll.PartesDelCuerpo[0].transform.position;
        posicion.y += 0.5f;
        posicion.z -= 0.5f;
        GameObject nuevaExplocion = (GameObject)Instantiate(this.Explosion, posicion, Quaternion.Euler(Vector3.up));
        nuevaExplocion.GetComponent<ParticleSystem>().Play();
        Destroy(nuevaExplocion, 8f);
    }

    /// <summary>
    /// Copia en los huesos de la animacion, la posicion y rotacion de los huesos del ragdoll.
    /// </summary>
    private void CopiarPoseRagdoll()
    {
        this.transform.position = this.ControladorRagdoll.PartesDelCuerpo[0].transform.position;
        /*this.ControladorRagdoll.PartesDelCuerpo[0].transform.position = new Vector3(0, 0, 0);

        for (int i = 0; i < this.PartesDelCuerpo.Length; i++)
        {
            this.partesDelCuerpo[i].transform.position = this.ControladorRagdoll.PartesDelCuerpo[i].transform.position;
            this.partesDelCuerpo[i].transform.rotation = new Quaternion(this.ControladorRagdoll.PartesDelCuerpo[i].transform.rotation.x, this.ControladorRagdoll.PartesDelCuerpo[i].transform.rotation.y, this.ControladorRagdoll.PartesDelCuerpo[i].transform.rotation.z, this.ControladorRagdoll.PartesDelCuerpo[i].transform.rotation.w);
        }
        this.PartesDelCuerpo[0].transform.position = new Vector3(0, 0, 0);*/
    }

    private Vector3 FuerzaImpactoContra(GameObject objetoImpactado)
    {
        var fuerzaImpactoPersonaje = (0.5f * this.RigidBody.mass) * Vector3.Scale(this.MovimientoJugador.VelocidadMovimiento, this.MovimientoJugador.VelocidadMovimiento);
        //Debug.Log("FUERZA IMPACTO PROPIA: " + fuerzaImpactoPersonaje.magnitude);
        var fuerzaImpactoExterna = (0.5f * objetoImpactado.GetComponent<Rigidbody>().mass) * Vector3.Scale(objetoImpactado.GetComponent<Rigidbody>().velocity, objetoImpactado.GetComponent<Rigidbody>().velocity);
       // Debug.Log("EXT: " + fuerzaImpactoExterna.magnitude);
        var fuerzaTotalImpacto = fuerzaImpactoExterna + fuerzaImpactoPersonaje;
       // Debug.Log(this.gameObject.name + ": " + fuerzaTotalImpacto.magnitude);
        return fuerzaTotalImpacto;
    }

    #region Collisiones
    void OnCollisionStay(Collision col)
    {
        if (col.gameObject.tag == "Piso")
        {
            this.EstaTocandoElPiso = true;
        }
    }

    void OnCollisionExit(Collision col)
    {
        this.EstaTocandoElPiso = false;
    }

    void OnCollisionEnter(Collision col)
    {
        //Si no colisiono contra el piso ni contra otro jugador (corriendo por ejemplo)
        if (col.gameObject.tag == "parteExplosion")
        {
            if (!this.MovimientoJugador.EstaAtacando())
            {
                Debug.Log("Collision");
                this.ControladorRagdoll.PrenderRagdoll();
                this.Partida.ActualizarGemas(this.Jugador);
            }
        }
        else if(col.gameObject.tag != "Piso" && col.gameObject.tag != "jugador")
        {
            if (col.gameObject.tag == "Gema" && !this.ControladorRagdoll.EstaActivadoRagdoll)
            {
                this.Jugador.AgarrarUnaGema(col.gameObject);
                this.Partida.ActualizarGemas(this.Jugador);
            }
            else if (col.gameObject.GetComponent<Rigidbody>() != null && this.FuerzaImpactoContra(col.gameObject).magnitude > 786)
            { 
                if (!this.MovimientoJugador.EstaAtacando())
                {
                    this.ControladorRagdoll.PrenderRagdoll();
                    this.Partida.ActualizarGemas(this.Jugador);
                }
            }
        }
        
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "parteExplosion")
        {
            if (!this.MovimientoJugador.EstaAtacando())
            {
                Debug.Log("Trigger");
                this.ControladorRagdoll.PrenderRagdoll();
                this.Partida.ActualizarGemas(this.Jugador);
            }
        }
        else if (col.gameObject.tag == "prop")
        {
            Debug.Log("El :" + this.gameObject.name + " toco el prop: " + col.gameObject.name);
        }
        else if (this.PuedeImpactarCon(col.gameObject) && this.EsDistintoQue(col.transform.root.gameObject) && this.MovimientoJugador.EstaAtacando())
        {
            //Debug.Log("El :" + this.gameObject.name + " toco: " + col.gameObject.name + " del: " + col.transform.root.gameObject.name);
            col.gameObject.GetComponent<Rigidbody>().AddForce(this.MovimientoJugador.VelocidadMovimiento * 1000f * ((col.transform.root.gameObject.GetComponentInChildren<Jugador>().PorcentajeVida/ 7.5f) + 1));
            col.transform.root.gameObject.GetComponent<CuerpoJugador>().Ragdoll.GetComponent<ControlRagdoll>().PrenderRagdoll();
            this.Partida.ActualizarGemas(this.Jugador);
        }
    }

    private bool EsDistintoQue(GameObject objeto)
    {
        return this.gameObject != objeto;
    }

    private bool PuedeImpactarCon(GameObject objeto)
    {
        return (objeto.tag == "jugador" || objeto.tag == "parteDelCuerpo");
    }
    #endregion

    #endregion

}
