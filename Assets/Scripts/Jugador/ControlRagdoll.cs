﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlRagdoll : MonoBehaviour
{
    #region ATRIBUTOS
    [SerializeField]
    private GameObject[] partesDelCuerpo;
    [SerializeField]
    private GameObject personajeAnimado;
    [SerializeField]
    private GameObject mesh;
    [SerializeField]
    private GameObject numeroPorcentaje;
    [SerializeField]
    public bool boton;
    #endregion

    #region PROPIEDADES
    public bool EstaActivadoRagdoll { private set; get; }
    private float TemporizadorRagdoll { set; get; }
    private float TiempoHastaLevantarse { set; get; }
    public float EstaDeEspaldas { private set; get; }
    public bool GuardarPosicionFinalRagdoll { set; get; }
    private int Iterador { set; get; }


    private Partida Partida { set; get; }
    private Jugador Jugador { set; get; }


    public GameObject[] PartesDelCuerpo
    {
        get
        {
            return partesDelCuerpo;
        }

        set
        {
            partesDelCuerpo = value;
        }
    }

    public GameObject PersonajeAnimado
    {
        get
        {
            return personajeAnimado;
        }

        set
        {
            personajeAnimado = value;
        }
    }

    public GameObject Mesh
    {
        get
        {
            return mesh;
        }

        set
        {
            mesh = value;
        }
    }

    public GameObject NumeroPorcentaje
    {
        get
        {
            return numeroPorcentaje;
        }

        set
        {
            numeroPorcentaje = value;
        }
    }
    #endregion

    #region METODOS UNITY

    // Use this for initialization
    void Start()
    {
        this.TemporizadorRagdoll = 0f;
        this.ApagarRagdoll();
        this.TiempoHastaLevantarse = 5f;
        this.EstaDeEspaldas = 0;
        this.Partida = GameObject.Find("Partida").GetComponent<Partida>();
        this.Jugador = this.GetComponentInParent<Jugador>();
    }

    // Update is called once per frame
    void Update()
    {
        if (boton)
            this.PrenderRagdoll();

        if (this.EstaActivadoRagdoll)
        {
            if (this.TemporizadorRagdoll > 0)
            {
                this.TemporizadorRagdoll -= Time.deltaTime;
                this.Jugador.PorcentajeVida += Time.deltaTime*2;
                this.Partida.TengoQueActualizarBarraVida = true;
            }
            else
            {
                this.Partida.TengoQueActualizarBarraVida = false;
                this.Levantarse();
            }
        }
    }
    #endregion

    #region METODOS
    public void PrenderRagdoll()
    {
        if (!this.EstaActivadoRagdoll)
        {
            //Copio la animacion antes de prenderse el ragdoll
            this.SeguirAnimacion();

            //Indico que esta prendido
            this.EstaActivadoRagdoll = true;

            //Indico cuanto tiempo hasta que se levante
            this.TemporizadorRagdoll = this.TiempoHastaLevantarse;

            //Desactivo al jugador
            this.PersonajeAnimado.GetComponent<CuerpoJugador>().Desactivarse();

            //Activo el mesh
            this.Mesh.GetComponent<SkinnedMeshRenderer>().enabled = true;

            //Activo el rigidbody y las fuerzas
            for (this.Iterador = 0; this.Iterador < this.PartesDelCuerpo.Length; this.Iterador++)
            {
                this.PartesDelCuerpo[this.Iterador].GetComponent<Rigidbody>().isKinematic = false;
                if (this.PartesDelCuerpo[this.Iterador].GetComponent<BoxCollider>() != null)
                    this.PartesDelCuerpo[this.Iterador].GetComponent<BoxCollider>().enabled = true;
                else if (this.PartesDelCuerpo[this.Iterador].GetComponent<CapsuleCollider>() != null)
                    this.PartesDelCuerpo[this.Iterador].GetComponent<CapsuleCollider>().enabled = true;
                else
                    this.PartesDelCuerpo[this.Iterador].GetComponent<SphereCollider>().enabled = true;

                this.PartesDelCuerpo[this.Iterador].GetComponent<Rigidbody>().AddForce(this.PersonajeAnimado.GetComponent<MovimientoJugador>().VelocidadMovimiento, ForceMode.VelocityChange);
            }

            //Dejo caer el sombrero
            this.PersonajeAnimado.GetComponentInChildren<Sombrero>().DesprenderSombrero();

            //Dejo caer las gemas
            this.PersonajeAnimado.GetComponentInChildren<Jugador>().TirarGemas();

        }
    }
    
    private void ApagarRagdoll()
    {

        //Indico que esta prendido
        this.EstaActivadoRagdoll = false;

        //Activo al jugador
        if (!this.PersonajeAnimado.GetComponent<CuerpoJugador>().EstaActivadoPersonaje)
            this.PersonajeAnimado.GetComponent<CuerpoJugador>().Activarse();

        //Desactivo el rigidbody y las fuerzas
        for (this.Iterador = 0; this.Iterador < this.PartesDelCuerpo.Length; this.Iterador++)
        {
            this.PartesDelCuerpo[this.Iterador].GetComponent<Rigidbody>().isKinematic = true;
            if (this.PartesDelCuerpo[this.Iterador].GetComponent<BoxCollider>() != null)
                this.PartesDelCuerpo[this.Iterador].GetComponent<BoxCollider>().enabled = false;
            else if (this.PartesDelCuerpo[this.Iterador].GetComponent<CapsuleCollider>() != null)
                this.PartesDelCuerpo[this.Iterador].GetComponent<CapsuleCollider>().enabled = false;
            else
                this.PartesDelCuerpo[this.Iterador].GetComponent<SphereCollider>().enabled = false;
        }

        //Desactivo el mesh
        this.Mesh.GetComponent<SkinnedMeshRenderer>().enabled = false;
    }

    private void SeguirAnimacion()
    {
        for (int i = 0; i < this.PartesDelCuerpo.Length; i++)
        {
            this.partesDelCuerpo[i].transform.position = this.PersonajeAnimado.GetComponent<CuerpoJugador>().PartesDelCuerpo[i].transform.position;
            this.partesDelCuerpo[i].transform.rotation = new Quaternion(this.PersonajeAnimado.GetComponent<CuerpoJugador>().PartesDelCuerpo[i].transform.rotation.x, this.PersonajeAnimado.GetComponent<CuerpoJugador>().PartesDelCuerpo[i].transform.rotation.y, this.PersonajeAnimado.GetComponent<CuerpoJugador>().PartesDelCuerpo[i].transform.rotation.z, this.PersonajeAnimado.GetComponent<CuerpoJugador>().PartesDelCuerpo[i].transform.rotation.w);
        }
    }

    private void Levantarse()
    {
        this.EstablecerPosicionLevantarse();
        this.ApagarRagdoll();
    }

    private void EstablecerPosicionLevantarse()
    {
        Vector3 hacia = this.PartesDelCuerpo[0].transform.TransformDirection(Vector3.forward);
        if (Physics.Raycast(this.PartesDelCuerpo[0].transform.position, hacia, 1))
            this.EstaDeEspaldas = 1f;
        else
        {
            hacia = this.PartesDelCuerpo[0].transform.TransformDirection(-Vector3.forward);

            if (Physics.Raycast(this.PartesDelCuerpo[0].transform.position, hacia, 1))
                this.EstaDeEspaldas = 0f;
        }

        this.GuardarPosicionFinalRagdoll = true;
    }
    #endregion
}
