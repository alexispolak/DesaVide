﻿using UnityEngine;
using System.Collections;

public class JugadorRigidBody : MonoBehaviour
{
    #region Variables (private)

    private bool grounded = false;
    private Vector3 groundVelocity;
    private CapsuleCollider capsule;

    // Inputs Cache
    private bool jumpFlag = false;

    #endregion

    #region Properties (public)

    // Speeds
    public float walkSpeed = 8.0f;
    public float walkBackwardSpeed = 4.0f;
    public float runSpeed = 14.0f;
    public float runBackwardSpeed = 6.0f;
    public float sidestepSpeed = 8.0f;
    public float runSidestepSpeed = 12.0f;
    public float maxVelocityChange = 10.0f;

    // Air
    public float inAirControl = 0.1f;
    public float jumpHeight = 2.0f;

    // Can Flags
    public bool canRunSidestep = true;
    public bool canJump = true;
    public bool canRun = true;

    #endregion

    #region ATRIBUTOS
    #endregion

    #region PROPIEDADES
    private bool EstaEnElPiso { set; get; }
    private Vector3 Velocidad { set; get; }
    private CapsuleCollider Capsula { set; get; }

    private bool FlagSalto { set; get; }

    //Velocidades
    private float VelocidadMovimiento {set;get;}
    private float MaximoCambioVelocidad {set;get;}
    private float ControlAereo {set;get;}
    private float AlturaSalto {set;get;}
    private float VelocidadRotacion {set;get;}
    private float VelocidadActual { set; get; }
    private float CambioVelocidad { set; get; }

    //Estados
    private bool PuedeSaltar {set; get;}
    #endregion

    #region METODOS UNITY
    // Use this for initialization
    void Awake()
    {
        this.Capsula = this.GetComponent<CapsuleCollider>();
    }

    void Start ()
    {
        this.CambioVelocidad = 0.2f;
        this.VelocidadActual = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void FixedUpdate()
    {
       /* var vectorInput = new Vector3(0, Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        if (this.EstaEnElPiso)
        {
            //Aplico fuerzas para intentar llegar a la velocidad objetivo
            var cambioDeVelocidad = CalcularCambioVelocidad(vectorInput);


            rigidbody.AddForce(velocityChange, ForceMode.VelocityChange);

            // Jump
            if (canJump && jumpFlag)
            {
                jumpFlag = false;
                rigidbody.velocity = new Vector3(rigidbody.velocity.x, rigidbody.velocity.y + CalculateJumpVerticalSpeed(), rigidbody.velocity.z);
            }

            // By setting the grounded to false in every FixedUpdate we avoid
            // checking if the character is not grounded on OnCollisionExit()
            grounded = false;
        }*/
    }
    #endregion

    #region METODOS
   /* private Vector3 CalcularCambioVelocidad(Vector3 vectorInput)
    {
        // Calculo que tan rapido tengo que moverme

        var velocidadRelativa = transform.TransformDirection(vectorInput);
        if (vectorInput.z > 0)
        {
            velocidadRelativa.z *= this.VelocidadMovimiento;
        }
       
        velocidadRelativa.x *= (canRunSidestep && Input.GetButton("Sprint")) ? runSidestepSpeed : sidestepSpeed;

        // Calcualte the delta velocity
        var currRelativeVelocity = rigidbody.velocity - groundVelocity;
        var velocityChange = relativeVelocity - currRelativeVelocity;
        velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
        velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
        velocityChange.y = 0;

        return velocityChange;
    }*/

    #endregion
}
