﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;

public class ControlRagdollV2 : MonoBehaviour
{
    #region ATRIBUTOS
    public bool ragdoll;


    #endregion


    #region PROPIEDADES
    private GameObject[] HuesosAnimacion { set; get; }
    private GameObject[] HuesosRagdoll { set; get; }
    private GameObject PersonajeAnimado { set; get; }
    private GameObject PersonajeRagdoll { set; get; }

    //Estados
    public bool EstaActivadoRagdoll { private set; get; }
    private float EstaDeEspaldas { set; get; }
    private bool GuardarPosicionFinalRagdoll { set; get; }

    //Temporizador
    private float TemporizadorHastaLevantarse { set; get; }
    private float TiempoEnRagdoll { set; get; }

    //Iteradores
    private int Iterador { set; get; }
    #endregion


    #region METODOS UNITY


	// Update is called once per frame
	void Update ()
    {
        this.ChequeoParaLevantarme();
    }

    #endregion

    #region METODOS

    #region inicializacion
    private void ObtenerHuesosAnimacion()
    {
        this.HuesosAnimacion = GameObject.FindGameObjectsWithTag("huesosAnimacion").OrderBy(go => go.name).ToArray();
    }

    private void ObtenerHuesosRagdoll()
    {
        this.HuesosRagdoll = GameObject.FindGameObjectsWithTag("huesosRagdoll").OrderBy(go => go.name).ToArray();
    }

    public void InicializarEstados()
    {
        this.ObtenerHuesosAnimacion();
        this.ObtenerHuesosRagdoll();

        this.TemporizadorHastaLevantarse = 0f;
        //this.ApagarRagdoll();
        this.TiempoEnRagdoll = 5f;
        this.EstaDeEspaldas = 0;
    }
    #endregion

    #region ActivarRagdoll
    /// <summary>
    /// Prende el ragdoll en el personaje
    /// </summary>
    public void PrenderRagdoll(GameObject personajeRagdoll)
    {
        if(this.PersonajeRagdoll == null)
            this.PersonajeRagdoll = personajeRagdoll;

        //Preparo para prender el ragdoll
        this.PrepararParaPrender();

        //Activo el rigidbody y las fuerzas
        this.ActivarRigidBodies(Vector3.zero);
    }

    /// <summary>
    /// Este metodo va a prender el ragdoll
    /// </summary>
    /// <param name="velocidadMovimiento"> Vector velocidad con la que entra al ragdoll </param>
    public void PrenderRagdoll(Vector3 velocidadMovimiento)
    {
        //Preparo para prender el ragdoll
        this.PrepararParaPrender();

        //Activo el rigidbody y las fuerzas
        this.ActivarRigidBodies(velocidadMovimiento);
    }

    /// <summary>
    /// Preparo lo necesario para prender el ragdoll
    /// </summary>
    private void PrepararParaPrender()
    {
        //Copio la animacion antes de prenderse el ragdoll
        this.CopiarAnimacion();

        //Indico que esta prendido
        this.EstaActivadoRagdoll = true;

        //Indico cuanto tiempo hasta que se levante
        this.TemporizadorHastaLevantarse = this.TiempoEnRagdoll;

        //Desactivo al personaje animado
        this.PersonajeAnimado.SetActive(false);

        //Activo al personaje ragdoll
        this.PersonajeRagdoll.SetActive(true);
    }

    /// <summary>
    /// Activa todos los colliders de los huesos
    /// </summary>
    /// <param name="velocidadMovimiento"></param>
    private void ActivarRigidBodies(Vector3 velocidadMovimiento)
    {
        for (this.Iterador = 0; this.Iterador < this.HuesosRagdoll.Length; this.Iterador++)
        {
            this.HuesosRagdoll[this.Iterador].GetComponent<Rigidbody>().isKinematic = false;
            if (this.HuesosRagdoll[this.Iterador].GetComponent<BoxCollider>() != null)
                this.HuesosRagdoll[this.Iterador].GetComponent<BoxCollider>().enabled = true;
            else if (this.HuesosRagdoll[this.Iterador].GetComponent<CapsuleCollider>() != null)
                this.HuesosRagdoll[this.Iterador].GetComponent<CapsuleCollider>().enabled = true;
            else
                this.HuesosRagdoll[this.Iterador].GetComponent<SphereCollider>().enabled = true;

            this.HuesosRagdoll[this.Iterador].GetComponent<Rigidbody>().AddForce(velocidadMovimiento, ForceMode.VelocityChange);
        }
    }

    /// <summary>
    /// Este metodo copia la animacion un instante antes de entrar a ragdoll para que comienze en esa posicion
    /// </summary>
    private void CopiarAnimacion()
    {
        for (int i = 0; i < this.HuesosRagdoll.Length; i++)
        {
            Debug.Log("An:" + this.HuesosAnimacion[i].gameObject.name + " Rag: " + this.HuesosRagdoll[i].gameObject.name);
            this.HuesosRagdoll[i].transform.position = this.HuesosAnimacion[i].transform.position;
            this.HuesosRagdoll[i].transform.rotation = new Quaternion(this.HuesosAnimacion[i].transform.rotation.x, this.HuesosAnimacion[i].transform.rotation.y, this.HuesosAnimacion[i].transform.rotation.z, this.HuesosAnimacion[i].transform.rotation.w);
        }
    }


    #endregion

    #region ApagarRagdoll
    /// <summary>
    /// Se utiliza para cuando se inicializa por primera vez, comience apagado
    /// </summary>
    public void InicializarEnApagado()
    {
        this.ApagarRagdoll();
    }

    /// <summary>
    /// Establece la posicion y apaga el ragdoll
    /// </summary>
    private void Levantarse()
    {

        this.PrepararParaApagar();

        this.ApagarRagdoll();
    }

    /// <summary>
    /// Preparo lo necesario para apagar el ragdoll
    /// </summary>
    private void PrepararParaApagar()
    {
        //Posiciono al personaje animado donde corresponde
        this.EstablecerPosicionLevantarse();

        //Desactivo al personaje ragdoll
        this.PersonajeRagdoll.SetActive(false);

        //Activo al personaje animado
        this.PersonajeAnimado.SetActive(true);

    }

    /// <summary>
    /// Apaga el ragdoll en el personaje
    /// </summary>
    private void ApagarRagdoll()
    {
        //Indico que esta prendido
        this.EstaActivadoRagdoll = false;

        //Desactivo el rigidbody y las fuerzas
        for (this.Iterador = 0; this.Iterador < this.HuesosRagdoll.Length; this.Iterador++)
        {
            if (this.HuesosRagdoll[this.Iterador].GetComponent<Rigidbody>() != null)
                this.HuesosRagdoll[this.Iterador].GetComponent<Rigidbody>().isKinematic = true;

            this.HuesosRagdoll[this.Iterador].GetComponent<Collider>().enabled = false;
        }
    }

    /// <summary>
    /// Establece la posicion del personaje animado donde termino el ragdoll
    /// </summary>
    private void EstablecerPosicionLevantarse()
    {
        Vector3 hacia = this.HuesosRagdoll[0].transform.TransformDirection(Vector3.forward);
        if (Physics.Raycast(this.HuesosRagdoll[0].transform.position, hacia, 1))
            this.EstaDeEspaldas = 1f;
        else
        {
            hacia = this.HuesosRagdoll[0].transform.TransformDirection(-Vector3.forward);

            if (Physics.Raycast(this.HuesosRagdoll[0].transform.position, hacia, 1))
                this.EstaDeEspaldas = 0f;
        }

        this.GuardarPosicionFinalRagdoll = true;
    }

    /// <summary>
    /// Chequea constantemente si esta en ragoll si se tiene que levantar
    /// </summary>
    private void ChequeoParaLevantarme()
    {
        if (this.EstaActivadoRagdoll)
        {
            if (this.TemporizadorHastaLevantarse > 0)
            {
                this.TemporizadorHastaLevantarse -= Time.deltaTime;
                Debug.Log("TENGO QUE SUMARLE AL PORCENTAJE DE VIDA EL Time.deltatime*2");
                Debug.Log("TENGO QUE ACTUALIZAR LA BARRA DE VIDA");
            }
            else
            {
                this.Levantarse();
            }
        }
    }
    #endregion

    public void AsignarPersonajeAnimado(GameObject personaje)
    {
        this.PersonajeAnimado = personaje;
    }

    #endregion
}
