﻿using UnityEngine;
using System.Collections;
using System;

public class CuerpoJugadorV2 : MonoBehaviour
{

    #region ATRIBUTOS
    #endregion

    #region PROPIEDADES
    //Animaciones
    private Animator ControlAnimaciones { set; get; }
    private MovimientoJugador MovimientoJugador { set; get; }

    //Componentes
    private Rigidbody RigidBody { set; get; }
    private CapsuleCollider Collider { set; get; }
    private Animator ControlAnimacionesHijo { set; get; }
    private AnimacionController ScriptControlAnimaciones { set; get; }

    //Estados
    public bool EstaActivadoPersonaje { set; get; }
    public bool EstaTocandoElPiso { set; get; }
    public bool TieneQueLevantarse { set; get; }

    #endregion

    #region METODOS UNITY

    // Use this for initialization
    void Awake ()
    {
        this.RigidBody = this.GetComponent<Rigidbody>();
        this.Collider = this.GetComponent<CapsuleCollider>();

    }
	
	// Update is called once per frame
	void Update () {
	
	}
    #endregion


    #region METODOS

    /// <summary>
    /// Desactiva todos los componentes para que tome el control el ragdoll
    /// </summary>
    public void Desactivarse()
    {
        this.RigidBody.isKinematic = true;
        this.Collider.enabled = false;

        this.ScriptControlAnimaciones.enabled = false;
        this.MovimientoJugador.enabled = false;
    }

    /// <summary>
    /// Activa todo los componentes luego que dejo de controlar el ragdoll
    /// </summary>
    public void Activarse()
    {
        this.TieneQueLevantarse = true;

        if (this.ControlAnimaciones != null)
            this.ControlAnimaciones.Play("Levantarse", 0);

        Debug.Log("Tengo que CopiarPoseRagdoll(posicion) ");

        this.ControlAnimacionesHijo.enabled = true;
        this.RigidBody.isKinematic = false;
        this.Collider.enabled = true;
        this.ScriptControlAnimaciones.enabled = true;
        this.MovimientoJugador.enabled = true;
    }

    /// <summary>
    /// Copia en los huesos de la animacion, la posicion y rotacion de los huesos del ragdoll.
    /// </summary>
    private void CopiarPoseRagdoll(Vector3 posicion)
    {
        this.transform.position = posicion;
    }

    private Vector3 FuerzaImpactoContra(GameObject objetoImpactado)
    {
        var fuerzaImpactoPersonaje = (0.5f * this.RigidBody.mass) * Vector3.Scale(this.MovimientoJugador.VelocidadMovimiento, this.MovimientoJugador.VelocidadMovimiento);
        //Debug.Log("FUERZA IMPACTO PROPIA: " + fuerzaImpactoPersonaje.magnitude);
        var fuerzaImpactoExterna = (0.5f * objetoImpactado.GetComponent<Rigidbody>().mass) * Vector3.Scale(objetoImpactado.GetComponent<Rigidbody>().velocity, objetoImpactado.GetComponent<Rigidbody>().velocity);
        // Debug.Log("EXT: " + fuerzaImpactoExterna.magnitude);
        var fuerzaTotalImpacto = fuerzaImpactoExterna + fuerzaImpactoPersonaje;
        // Debug.Log(this.gameObject.name + ": " + fuerzaTotalImpacto.magnitude);
        return fuerzaTotalImpacto;
    }

    #endregion

    #region COLISIONES

    void OnCollisionEnter(Collision col)
    {
        Debug.Log("Hijo: " + col.gameObject.name);
    }

    #endregion


}

