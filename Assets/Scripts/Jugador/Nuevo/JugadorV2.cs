﻿using UnityEngine;
using System.Collections;

public class JugadorV2 : MonoBehaviour
{
    #region ATRIBUTOS
    public bool activarRagdoll;

    [SerializeField]
    private GameObject personajeAnimado;

    [SerializeField]
    private GameObject personajeRagdoll;
    #endregion

    #region PROPIEDADES
    public float PorcentajeVida { set; get; }
    private ArrayList Gemas { set; get; }
    private int NumeroJugador { set; get; }

    //Componentes
    public CuerpoJugadorV2 CuerpoJugador { private set; get; }
    private ControlRagdollV2 ControlRagdoll { set; get; }
    private MovimientoJugador MovimientoJugador { set; get; }
    private AnimacionController AnimacionController { set; get; }
    private Rigidbody RigidBody { set; get; }

    #region propiedades de atributos
    public GameObject PersonajeAnimado
    {
        get
        {
            return personajeAnimado;
        }

        set
        {
            personajeAnimado = value;
        }
    }

    public GameObject PersonajeRagdoll
    {
        get
        {
            return personajeRagdoll;
        }

        set
        {
            personajeRagdoll = value;
        }
    }
    #endregion

    #endregion

    #region METODOS UNITY
    #endregion

    // Use this for initialization
    void Start ()
    {
        this.InicializarComponentes();
        this.NumeroJugador = 1;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (this.activarRagdoll)
        {
            this.ControlRagdoll.PrenderRagdoll(this.PersonajeRagdoll);
            this.activarRagdoll = false;
        }
	}

    #region METODOS

    private void InicializarComponentes()
    {
        this.PorcentajeVida = 0;
        this.Gemas = new ArrayList();

        this.MovimientoJugador = this.GetComponent<MovimientoJugador>();
        this.AnimacionController = this.GetComponentInChildren<AnimacionController>();
        this.RigidBody = this.GetComponentInChildren<Rigidbody>();

        this.ControlRagdoll = this.gameObject.AddComponent<ControlRagdollV2>();
        this.ControlRagdoll.AsignarPersonajeAnimado(this.PersonajeAnimado);
        this.ControlRagdoll.InicializarEstados();
        //this.PersonajeRagdoll.SetActive(false);
        

        this.CuerpoJugador = this.gameObject.AddComponent<CuerpoJugadorV2>();

        this.ControlRagdoll.InicializarEnApagado();
    }

    #region GEMAS
    /// <summary>
    /// Agarra una gema, la agrega a la coleccion y la mueve fuera de la camara
    /// </summary>
    /// <param name="unaGema"></param>
    public void AgarrarUnaGema(GameObject unaGema)
    {
        this.Gemas.Add(unaGema);
        unaGema.GetComponent<GiroGema>().MoverFueraDelMapa();
    }
    #endregion

    #endregion

    #region COLISIONES

    private Vector3 FuerzaImpactoContra(GameObject objetoImpactado)
    {
        var fuerzaImpactoPersonaje = (0.5f * this.RigidBody.mass) * Vector3.Scale(this.MovimientoJugador.VelocidadMovimiento, this.MovimientoJugador.VelocidadMovimiento);
        //Debug.Log("FUERZA IMPACTO PROPIA: " + fuerzaImpactoPersonaje.magnitude);
        var fuerzaImpactoExterna = (0.5f * objetoImpactado.GetComponent<Rigidbody>().mass) * Vector3.Scale(objetoImpactado.GetComponent<Rigidbody>().velocity, objetoImpactado.GetComponent<Rigidbody>().velocity);
        // Debug.Log("EXT: " + fuerzaImpactoExterna.magnitude);
        var fuerzaTotalImpacto = fuerzaImpactoExterna + fuerzaImpactoPersonaje;
        // Debug.Log(this.gameObject.name + ": " + fuerzaTotalImpacto.magnitude);
        return fuerzaTotalImpacto;
    }

    private void ChequeoEstaTocandoElPiso(Collision col)
    {
        if (col.gameObject.tag == "Piso")
        {
            this.CuerpoJugador.EstaTocandoElPiso = true;
        }
    }

    void OnCollisionStay(Collision col)
    {
        this.ChequeoEstaTocandoElPiso(col);
    }

    void OnCollisionExit(Collision col)
    {
        if (col.gameObject.tag == "Piso")
        {
            this.CuerpoJugador.EstaTocandoElPiso = false;
        }

        //this.EstaTocandoElPiso = false;
    }

    void OnCollisionEnter(Collision col)
    {
        //Si no colisiono contra el piso ni contra otro jugador (corriendo por ejemplo)
        if (col.gameObject.tag == "parteExplosion")
        {
            if (!this.MovimientoJugador.EstaAtacando())
            {
                Debug.Log("Collision");
                this.ControlRagdoll.PrenderRagdoll(this.PersonajeRagdoll);
                //this.Partida.ActualizarGemas(this.Jugador);
            }
        }
        else if (col.gameObject.tag != "Piso" && col.gameObject.tag != "jugador")
        {
            if (col.gameObject.tag == "Gema" && !this.ControlRagdoll.EstaActivadoRagdoll)
            {
                this.AgarrarUnaGema(col.gameObject);
                //this.Partida.ActualizarGemas(this.Jugador);
            }
            else if (col.gameObject.GetComponent<Rigidbody>() != null && this.FuerzaImpactoContra(col.gameObject).magnitude > 786)
            {
                if (!this.MovimientoJugador.EstaAtacando())
                {
                    this.ControlRagdoll.PrenderRagdoll(this.PersonajeRagdoll);
                    //this.Partida.ActualizarGemas(this.Jugador);
                }
            }
        }

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "parteExplosion")
        {
            if (!this.MovimientoJugador.EstaAtacando())
            {
                Debug.Log("Trigger");
                this.ControlRagdoll.PrenderRagdoll(this.PersonajeRagdoll);
                //this.Partida.ActualizarGemas(this.Jugador);
            }
        }
        else if (col.gameObject.tag == "prop")
        {
            Debug.Log("El :" + this.gameObject.name + " toco el prop: " + col.gameObject.name);
        }
        else if (this.PuedeImpactarCon(col.gameObject) && this.EsDistintoQue(col.transform.root.gameObject) && this.MovimientoJugador.EstaAtacando())
        {
            Debug.Log("El :" + this.gameObject.name + " toco: " + col.gameObject.name + " del: " + col.transform.root.gameObject.name);
            col.gameObject.GetComponent<Rigidbody>().AddForce(this.MovimientoJugador.VelocidadMovimiento * 1000f * ((col.transform.root.gameObject.GetComponentInChildren<Jugador>().PorcentajeVida / 7.5f) + 1));
            col.transform.root.gameObject.GetComponent<CuerpoJugador>().Ragdoll.GetComponent<ControlRagdoll>().PrenderRagdoll();
            //this.Partida.ActualizarGemas(this.Jugador);
        }
    }

    private bool EsDistintoQue(GameObject objeto)
    {
        return this.gameObject != objeto;
    }

    private bool PuedeImpactarCon(GameObject objeto)
    {
        return (objeto.tag == "jugador" || objeto.tag == "parteDelCuerpo");
    }
    #endregion


}
