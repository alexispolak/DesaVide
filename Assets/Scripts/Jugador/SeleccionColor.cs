﻿using UnityEngine;
using System.Collections;

public class SeleccionColor : MonoBehaviour
{
    public bool usoColorInicial;
    public bool aplicarCambios;

    [ColorUsageAttribute(false, false, 0f, 0f, 0f, 1f)]
    public Color colorRemera;

    [ColorUsageAttribute(false, false, 0f, 0f, 0f, 1f)]
    public Color colorPantalon;

    [ColorUsageAttribute(false, false, 0f, 0f, 0f, 1f)]
    public Color colorZapatillas;

    [ColorUsageAttribute(false, false, 0f, 0f, 0f, 1f)]
    public Color colorPiel;

    // Use this for initialization
    void Start ()
    {
        TomarColorInicial();
        this.aplicarCambios = true;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(this.aplicarCambios)
        {
            this.CambiarColor();
        }
    }

    private void TomarColorInicial()
    {
        if(this.usoColorInicial)
        { 
            colorPiel = this.GetComponentInChildren<SkinnedMeshRenderer>().materials[0].color;
            colorRemera = this.GetComponentInChildren<SkinnedMeshRenderer>().materials[1].color;
            colorPantalon = this.GetComponentInChildren<SkinnedMeshRenderer>().materials[2].color;
            colorZapatillas =this.GetComponentInChildren<SkinnedMeshRenderer>().materials[3].color;
        }
    }

    private void CambiarColor()
    {
        
        this.GetComponentsInChildren<SkinnedMeshRenderer>()[0].materials[0].color = colorPiel;
        this.GetComponentsInChildren<SkinnedMeshRenderer>()[0].materials[1].color = colorRemera;
        this.GetComponentsInChildren<SkinnedMeshRenderer>()[0].materials[2].color = colorPantalon;
        this.GetComponentsInChildren<SkinnedMeshRenderer>()[0].materials[3].color = colorZapatillas;

        if (this.GetComponentsInChildren<SkinnedMeshRenderer>().Length > 1)
        { 
            this.GetComponentsInChildren<SkinnedMeshRenderer>()[1].materials[0].color = colorPiel;
            this.GetComponentsInChildren<SkinnedMeshRenderer>()[1].materials[1].color = colorRemera;
            this.GetComponentsInChildren<SkinnedMeshRenderer>()[1].materials[2].color = colorPantalon;
            this.GetComponentsInChildren<SkinnedMeshRenderer>()[1].materials[3].color = colorZapatillas;
        }

        this.aplicarCambios = false;
    }
}
