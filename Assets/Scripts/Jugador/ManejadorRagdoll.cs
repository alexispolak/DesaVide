﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ManejadorRagdoll : MonoBehaviour
{
    #region ATRITBUTOS
    public float aguanteFuerzaImpactos;
    private float ultimaFuerzaImpacto; //TODO borrar
    private float fuerzaMasFuerte;

    public Material materialdebug;

    [SerializeField]
    private GameObject pelvis;
    [SerializeField]
    private GameObject espalda;
    [SerializeField]
    private GameObject cabeza;
    [SerializeField]
    private GameObject piernaDerecha;
    [SerializeField]
    private GameObject pantorillaDerecha;
    [SerializeField]
    private GameObject piernaIzquierda;
    [SerializeField]
    private GameObject pantorillaIzquierda;
    [SerializeField]
    private GameObject brazoDerecho;
    [SerializeField]
    private GameObject antebrazoDerecho;
    [SerializeField]
    private GameObject brazoIzquierdo;
    [SerializeField]
    private GameObject antebrazoIzquierdoo;


    public bool matar;
    private CapsuleCollider ColliderGeneral { set; get; }
    #endregion

    #region PROPIEDADES
    public bool EstaActivadoRagdoll { private set; get; }
    private Vector3 PosicionAnterior { set; get; }
    private Vector3 Velocidad { set; get; }
    private Vector3 VelocidadAnterior { set; get; }
    private Vector3 Aceleracion { set; get; }
    private ArrayList PartesDelCuerpo { set; get; }
    private Vector3 UltimaFuerzaImpacto { set; get; }
    private float TemporizadorRagdoll { set; get; }

    public GameObject Pelvis
    {
        get
        {
            return pelvis;
        }

        set
        {
            pelvis = value;
        }
    }

    public GameObject Espalda
    {
        get
        {
            return espalda;
        }

        set
        {
            espalda = value;
        }
    }

    public GameObject Cabeza
    {
        get
        {
            return cabeza;
        }

        set
        {
            cabeza = value;
        }
    }

    public GameObject PiernaDerecha
    {
        get
        {
            return piernaDerecha;
        }

        set
        {
            piernaDerecha = value;
        }
    }

    public GameObject PantorillaDerecha
    {
        get
        {
            return pantorillaDerecha;
        }

        set
        {
            pantorillaDerecha = value;
        }
    }

    public GameObject PiernaIzquierda
    {
        get
        {
            return piernaIzquierda;
        }

        set
        {
            piernaIzquierda = value;
        }
    }

    public GameObject PantorillaIzquierda
    {
        get
        {
            return pantorillaIzquierda;
        }

        set
        {
            pantorillaIzquierda = value;
        }
    }

    public GameObject BrazoDerecho
    {
        get
        {
            return brazoDerecho;
        }

        set
        {
            brazoDerecho = value;
        }
    }

    public GameObject AntebrazoDerecho
    {
        get
        {
            return antebrazoDerecho;
        }

        set
        {
            antebrazoDerecho = value;
        }
    }

    public GameObject BrazoIzquierdo
    {
        get
        {
            return brazoIzquierdo;
        }

        set
        {
            brazoIzquierdo = value;
        }
    }

    public GameObject AntebrazoIzquierdoo
    {
        get
        {
            return antebrazoIzquierdoo;
        }

        set
        {
            antebrazoIzquierdoo = value;
        }
    }
    #endregion

    #region METODOS UNITY
    void Start()
    {
        this.PartesDelCuerpo = new ArrayList();
        this.GuardarPartesDelCuerpo();
        this.ColliderGeneral = this.gameObject.GetComponent<CapsuleCollider>();
        this.TemporizadorRagdoll = 5f;
    }   

    void Update()
    {
        this.MRUV();

        GameObject.Find("Text").GetComponent<Text>().text = "Velocidad: "+this.Velocidad.magnitude.ToString("00")+"\nAceleracion: "+this.Aceleracion.magnitude.ToString("00") + "\nFuerza mas Fuerte:"+this.fuerzaMasFuerte;

        if (this.EstaActivadoRagdoll)
        {
            if (this.TemporizadorRagdoll > 0)
            {
                this.TemporizadorRagdoll -= Time.deltaTime;
            }
            else
            {
                this.Pararse();
            }
            
            
        }
    }
    #endregion


    #region METODOS
    private void GuardarPartesDelCuerpo()
    {
        this.PartesDelCuerpo.Add(this.Pelvis);
        this.PartesDelCuerpo.Add(this.Cabeza);
        this.PartesDelCuerpo.Add(this.Espalda);
        this.PartesDelCuerpo.Add(this.PiernaDerecha);
        this.PartesDelCuerpo.Add(this.PiernaIzquierda);
        this.PartesDelCuerpo.Add(this.PantorillaDerecha);
        this.PartesDelCuerpo.Add(this.PantorillaIzquierda);
        this.PartesDelCuerpo.Add(this.BrazoIzquierdo);
        this.PartesDelCuerpo.Add(this.BrazoDerecho);
        this.PartesDelCuerpo.Add(this.AntebrazoDerecho);
        this.PartesDelCuerpo.Add(this.antebrazoIzquierdoo);
    }

    public void ApagarRagdoll()
    {
        if (this.ColliderGeneral == null)
            this.ColliderGeneral = this.gameObject.GetComponent<CapsuleCollider>();

        //Indico que esta apagado
        this.EstaActivadoRagdoll = false;

        //Prendo el collider general y su rigidBody
        this.ColliderGeneral.enabled = true;
        this.gameObject.GetComponent<Rigidbody>().isKinematic = false;

        //Prendo el animador y los controles
        this.gameObject.GetComponent<Animator>().enabled = true;
        //this.gameObject.GetComponent<Jugador>().activarControles = true;

        //Apago las fisicas
        this.Pelvis.GetComponent<Rigidbody>().isKinematic = true;
        this.Cabeza.GetComponent<Rigidbody>().isKinematic = true;
        this.Espalda.GetComponent<Rigidbody>().isKinematic = true;
        this.PiernaDerecha.GetComponent<Rigidbody>().isKinematic = true;
        this.PiernaIzquierda.GetComponent<Rigidbody>().isKinematic = true;
        this.PantorillaDerecha.GetComponent<Rigidbody>().isKinematic = true;
        this.PantorillaIzquierda.GetComponent<Rigidbody>().isKinematic = true;
        this.BrazoIzquierdo.GetComponent<Rigidbody>().isKinematic = true;
        this.BrazoDerecho.GetComponent<Rigidbody>().isKinematic = true;
        this.AntebrazoDerecho.GetComponent<Rigidbody>().isKinematic = true;
        this.antebrazoIzquierdoo.GetComponent<Rigidbody>().isKinematic = true;

        //Apago las colisiones
        /*this.Pelvis.GetComponent<BoxCollider>().enabled = false;
        this.Cabeza.GetComponent<SphereCollider>().enabled = false;
        this.Espalda.GetComponent<BoxCollider>().enabled = false;
        this.PiernaDerecha.GetComponent<CapsuleCollider>().enabled = false;
        this.PiernaIzquierda.GetComponent<CapsuleCollider>().enabled = false;
        this.PantorillaDerecha.GetComponent<CapsuleCollider>().enabled = false;
        this.PantorillaIzquierda.GetComponent<CapsuleCollider>().enabled = false;
        this.BrazoIzquierdo.GetComponent<CapsuleCollider>().enabled = false;
        this.BrazoDerecho.GetComponent<CapsuleCollider>().enabled = false;
        this.AntebrazoDerecho.GetComponent<BoxCollider>().enabled = false;
        this.antebrazoIzquierdoo.GetComponent<BoxCollider>().enabled = false;*/
    }

    public void PrenderRagdoll()
    {

        //Indico que esta prendido
        this.EstaActivadoRagdoll = true;

        //Apago el collider general y su rigidBody
        this.ColliderGeneral.enabled = false;
        this.gameObject.GetComponent<Rigidbody>().isKinematic = true;

        //Apago el animador
        this.gameObject.GetComponent<Animator>().enabled = false;

        //Prendo las fisicas
        this.Pelvis.GetComponent<Rigidbody>().isKinematic = false;
        this.Cabeza.GetComponent<Rigidbody>().isKinematic = false;
        this.Espalda.GetComponent<Rigidbody>().isKinematic = false;
        this.PiernaDerecha.GetComponent<Rigidbody>().isKinematic = false;
        this.PiernaIzquierda.GetComponent<Rigidbody>().isKinematic = false;
        this.PantorillaDerecha.GetComponent<Rigidbody>().isKinematic = false;
        this.PantorillaIzquierda.GetComponent<Rigidbody>().isKinematic = false;
        this.BrazoIzquierdo.GetComponent<Rigidbody>().isKinematic = false;
        this.BrazoDerecho.GetComponent<Rigidbody>().isKinematic = false;
        this.AntebrazoDerecho.GetComponent<Rigidbody>().isKinematic = false;
        this.antebrazoIzquierdoo.GetComponent<Rigidbody>().isKinematic = false;

        //Les agrego una fuerza
        foreach (GameObject parteDelCuerpo in this.PartesDelCuerpo)
        {
            this.AsignarVelocidadAObjetos(parteDelCuerpo);
        }

        //Prendo las colisiones
        this.Pelvis.GetComponent<BoxCollider>().enabled = true;
        this.Cabeza.GetComponent<SphereCollider>().enabled = true;
        this.Espalda.GetComponent<BoxCollider>().enabled = true;
        this.PiernaDerecha.GetComponent<CapsuleCollider>().enabled = true;
        this.PiernaIzquierda.GetComponent<CapsuleCollider>().enabled = true;
        this.PantorillaDerecha.GetComponent<CapsuleCollider>().enabled = true;
        this.PantorillaIzquierda.GetComponent<CapsuleCollider>().enabled = true;
        this.BrazoIzquierdo.GetComponent<CapsuleCollider>().enabled = true;
        this.BrazoDerecho.GetComponent<CapsuleCollider>().enabled = true;
        this.AntebrazoDerecho.GetComponent<BoxCollider>().enabled = true;
        this.antebrazoIzquierdoo.GetComponent<BoxCollider>().enabled = true;
    }

    private void MRUV()
    {
        this.CalcularVelocidadActual();
        this.CalcularAceleracionActual();
        this.PosicionAnterior = this.transform.position;
        this.VelocidadAnterior = this.Velocidad;
    }

    private void CalcularVelocidadActual()
    {
        this.Velocidad = (this.transform.position - this.PosicionAnterior) / Time.deltaTime;
    }

    private void CalcularAceleracionActual()
    {
        this.Aceleracion = (this.Velocidad - this.VelocidadAnterior) / Time.deltaTime;
    }

    private void AsignarVelocidadAObjetos(GameObject objeto)
    {
       objeto.GetComponent<Rigidbody>().AddForce(this.Velocidad, ForceMode.Impulse);
    }

    public Vector3 FuerzaImpacto(GameObject objeto, GameObject objetoCol)
    {
        var fuerzaImpactoPropia = (0.5f * objeto.GetComponent<Rigidbody>().mass) * (Vector3.Scale(this.Velocidad, this.Velocidad));
        var fuerzaImpactoExterna = (0.5f * objetoCol.GetComponent<Rigidbody>().mass) * (Vector3.Scale(objetoCol.GetComponent<Rigidbody>().velocity, objetoCol.GetComponent<Rigidbody>().velocity));
        var fuerzaImpactoTotal = fuerzaImpactoPropia + fuerzaImpactoExterna;
        Debug.Log("N: "+(fuerzaImpactoPropia).magnitude+" + "+ (fuerzaImpactoExterna).magnitude+" = "+ (fuerzaImpactoTotal).magnitude);
        Debug.Log("----");
        if (this.fuerzaMasFuerte < (fuerzaImpactoTotal).magnitude)
            this.fuerzaMasFuerte = (fuerzaImpactoTotal).magnitude;

        this.UltimaFuerzaImpacto = fuerzaImpactoTotal;
        return fuerzaImpactoTotal;
    }

    public void DetectarFuerzaImpacto(GameObject parteDelCuerpo, GameObject objetoColisionado)
    {
        if (this.aguanteFuerzaImpactos < this.FuerzaImpacto(parteDelCuerpo, objetoColisionado).magnitude)
        {
            if (!this.EstaActivadoRagdoll)
                this.PrenderRagdoll();
            
            parteDelCuerpo.GetComponent<Rigidbody>().AddForce(this.Velocidad * -1, ForceMode.Impulse);
        }
    }

    private void Pararse()
    {
        this.ApagarRagdoll();
    }
    #endregion

}
