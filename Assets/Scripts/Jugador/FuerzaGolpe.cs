﻿using UnityEngine;
using System.Collections;

public class FuerzaGolpe : MonoBehaviour
{
    #region ATRIBUTOS
    [SerializeField]
    private GameObject padre;
    #endregion

    #region PROPIEDADES
    private Vector3 PosicionAnterior { set; get; }
    private Vector3 VelocidadGolpe { set; get; }
    private Vector3 Aceleracion { set; get; }
    private Vector3 VelocidadAnterior { set; get; }
    private Vector3 FuerzaDeGolpe { set; get; }
    private float Masa { set; get; }

    public GameObject Padre
    {
        get
        {
            return padre;
        }

        set
        {
            padre = value;
        }
    }
    #endregion

    #region METODOS UNITY
    // Use this for initialization
    void Start ()
    {
        this.Masa = this.GetComponent<Rigidbody>().mass;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void FixedUpdate()
    {
        this.CalcularVelocidadMano();
        this.CalcularAceleracionMano();
        this.VelocidadAnterior = this.VelocidadGolpe;
        this.CalcularFuerzaGolpe();
    }
    #endregion

    #region METODOS
    private void CalcularVelocidadMano()
    {
        this.VelocidadGolpe = (this.transform.position - this.PosicionAnterior) / Time.deltaTime;
    }

    private void CalcularAceleracionMano()
    {
        this.Aceleracion = (this.VelocidadGolpe - this.VelocidadAnterior) / Time.deltaTime;
    }

    private void CalcularFuerzaGolpe()
    {
        this.FuerzaDeGolpe = this.Aceleracion * this.Masa;
    }

    void OnCollisionEnter(Collision col)
    {
       /* 
        if (col.gameObject.GetComponent<Rigidbody>() != null || col.transform.parent.gameObject != this.transform.parent.gameObject)
        {
            col.gameObject.GetComponent<Rigidbody>().AddForce((this.Aceleracion * this.Masa), ForceMode.Impulse);
        }*/
    }
    #endregion

}
