﻿using UnityEngine;
using System.Collections;
using System;

public class MovimientoJugador : MonoBehaviour
{
    #region ATRIBUTOS
    [SerializeField]
    private GameObject personajeAnimacion;

    public bool desactivarJugador;
    #endregion

    #region PROPIEDADES
    private Animator Animador { set; get; }
    private Rigidbody RigidBody { set; get; }

    public Vector3 VelocidadMovimiento { private set; get; }
    private Vector3 UltimaRotacion { set; get; }
    private CuerpoJugador CuerpoJugador { set; get; }
    private CuerpoJugadorV2 CuerpoJugadorV2 { set; get; }

    public bool AccionSaltar { private set; get; }
    public bool AccionGolpeI { private set; get; }
    public bool AccionGolpeD { private set; get; }
    public bool AccionPatada { private set; get; }
    public bool AccionTirar { private set; get; }
    public bool AccionBarrida { private set; get; }

    public GameObject PersonajeAnimacion
    {
        get
        {
            return personajeAnimacion;
        }

        set
        {
            personajeAnimacion = value;
        }
    }

    #region CONTROLES
    public int NumeroJugador { set; get; }
    public string J_Horizontal { set; get; }
    public string J_Vertical { set; get; }
    public string J_Correr { set; get; }
    public string J_Saltar { set; get; }
    public string J_Patada { set; get; }
    public string J_Options { set; get; }
    public string J_Barrida { set; get; }
    #endregion

    #endregion

    #region METODOS UNITY
    // Use this for initialization
    void Start ()
    {
        this.CuerpoJugador = this.GetComponent<CuerpoJugador>();
        this.Animador = this.GetComponentInChildren<Animator>();
        this.RigidBody = this.GetComponentInChildren<Rigidbody>();
    }
	
	// Update is called once per frame
	void LateUpdate ()
    {
        if (!this.desactivarJugador)
        { 
            if(this.Animador.GetBool("puedeMoverse"))
            { 
                this.Movimiento();

                this.Acciones();
            }
        }

    }
    #endregion

    #region METODOS

    private void Movimiento()
    {
        this.VelocidadMovimiento = new Vector3(Input.GetAxis(this.J_Horizontal), 0, Input.GetAxis(this.J_Vertical));

        if (this.VelocidadMovimiento.magnitude > 0)
        {
            this.VelocidadMovimiento *= 1 + (Input.GetAxis(this.J_Correr) * 5f);
            this.transform.Translate(VelocidadMovimiento * 0.025f);

            this.Rotacion();
        }
    }

    private void Rotacion()
    {
        this.PersonajeAnimacion.transform.rotation = Quaternion.Lerp(this.PersonajeAnimacion.transform.rotation, Quaternion.LookRotation(this.VelocidadMovimiento), 0.35f);
    }

    private void Acciones()
    {
        //TODO CAMBIAR ESTO
        if(this.CuerpoJugadorV2 != null)
        {
            this.Salto();

            this.Patada();

            this.Barrida();
        }
        else if(this.CuerpoJugador.EstaTocandoElPiso)
        { 
            this.Salto();

            this.Patada();

            this.Barrida();
        }
    }

    private void Salto()
    {

        this.AccionSaltar = Input.GetButtonDown(this.J_Saltar);

        if (this.AccionSaltar)
            this.RigidBody.AddForce(Vector3.up * 250f, ForceMode.Impulse);

    }

    private void Patada()
    {

        this.AccionPatada = Input.GetButtonDown(this.J_Patada);

        if(this.AccionPatada && this.VelocidadMovimiento.magnitude > 1.41f)
            this.RigidBody.AddForce(Vector3.up * 250f, ForceMode.Impulse);
    }

    private void Barrida()
    {
        this.AccionBarrida = Input.GetButtonDown(this.J_Barrida);

        if (this.AccionBarrida && this.VelocidadMovimiento.magnitude > 1.41f)
            this.RigidBody.AddForce(Vector3.forward * 75f, ForceMode.Impulse);
    }


    public bool EstaAtacando()
    {
        
        if (this.AccionBarrida || this.AccionPatada || this.AccionGolpeD || this.AccionGolpeI || this.AccionTirar )
            return true;
        else
            return false;
    }
    #endregion
}


