﻿using UnityEngine;
using System.Collections;

public class DeteccionImpacto : MonoBehaviour
{
    #region ATRIBUTOS
    [SerializeField]
    private GameObject padre;
    #endregion

    #region PROPIEDADES
    public GameObject Padre
    {
        get
        {
            return padre;
        }

        set
        {
            padre = value;
        }
    }
    #endregion

    #region METODOS UNITY
    // Use this for initialization
    void Start ()
    {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    #endregion

    #region METODOS
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag != "Piso")
        {
            this.Padre.GetComponent<ManejadorRagdoll>().DetectarFuerzaImpacto(this.gameObject, col.gameObject);
        }
    }
    #endregion

}
