﻿using UnityEngine;
using System.Collections;

public class PropiedadesSombrero : MonoBehaviour {

	
    public Vector2 Desplazamiento { set; get; }
    public Vector3 DesplazamientoRot { set; get; }
    public float dX;
    public float dY;
    public float rotX;
    public float rotY;
    public float rotZ;

    // Use this for initialization
	void Start ()
    {
        if(!this.dX.Equals(null) || !this.dY.Equals(null))
            this.Desplazamiento = new Vector2(dX,dY);
        else
            this.Desplazamiento = new Vector2(0, 0);

        if (!this.rotX.Equals(null) || !this.rotY.Equals(null) || !this.rotZ.Equals(null))
            this.DesplazamientoRot = new Vector3(rotX, rotY, rotZ);
        else
            this.DesplazamientoRot = new Vector2(0, 0);

    }
}
