﻿using UnityEngine;
using System.Collections;
using System;

public class Sombrero : MonoBehaviour
{

    #region ATRIBUTOS
    public GameObject sombreroPrueba;
    #endregion

    #region PROPIEDADES
    public GameObject Modelo { set; get; }
    private Vector2 DesplazamientoGalera { set; get; }
    private bool TengoQuePosicionar { set; get; }
    public bool TengoSombrero { private set; get; }
    #endregion

    #region METODOS UNITY
    
    // Use this for initialization
    void Start ()
    {
        this.TengoQuePosicionar = true;
        this.TengoSombrero = true;

        if (this.sombreroPrueba != null)
            this.Modelo = sombreroPrueba;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(this.TengoQuePosicionar)
            this.PosicionarEnCabeza();
    }


    #endregion

    #region ATRIBUMETODOSTOS
    private void PosicionarEnCabeza()
    {
        var nuevaPosicion = this.transform.position;
        var nuevaRotacion = this.transform.rotation;

        nuevaPosicion.y -= this.Modelo.GetComponent<PropiedadesSombrero>().Desplazamiento.x;
        nuevaPosicion.z -= this.Modelo.GetComponent<PropiedadesSombrero>().Desplazamiento.y;

        nuevaRotacion = Quaternion.Euler(new Vector3(nuevaRotacion.eulerAngles.x + this.Modelo.GetComponent<PropiedadesSombrero>().DesplazamientoRot.x, 
                                                     nuevaRotacion.eulerAngles.y + this.Modelo.GetComponent<PropiedadesSombrero>().DesplazamientoRot.y,
                                                     nuevaRotacion.eulerAngles.z + this.Modelo.GetComponent<PropiedadesSombrero>().DesplazamientoRot.z));

        this.Modelo.transform.position = nuevaPosicion;
        this.Modelo.transform.rotation = nuevaRotacion;
    }

    public void DesprenderSombrero()
    {
        this.TengoQuePosicionar = false;
        this.Modelo.GetComponent<Rigidbody>().isKinematic = false;
        this.Modelo.GetComponent<Collider>().enabled = true;
        this.TengoSombrero = false;
    }
    #endregion

}
