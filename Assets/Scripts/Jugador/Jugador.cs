﻿using UnityEngine;
using System.Collections;
using System;

public class Jugador : MonoBehaviour
{
    #region ATRIBUTOS
    [SerializeField]
    private int vidas;
    #endregion

    #region PROPIEDADES
    private int Asesinatos { set; get; }
    private int ObjetosDestruidos { set; get; }
    private bool TieneSombrero { set; get; }
    public int NumeroJugador { set; get; }
    public float PorcentajeVida { set; get; }
    private ArrayList Gemas { set; get; }

    public int Vidas
    {
        get
        {
            return vidas;
        }

        set
        {
            vidas = value;
        }
    }

    #endregion

    #region METODOS UNITY

    // Use this for initialization
    void Start()
    {
        this.PorcentajeVida = 0;
        this.Gemas = new ArrayList();
        
    }

    #endregion

    #region METODOS

    public void AgarrarUnaGema(GameObject unaGema)
    {
        this.Gemas.Add(unaGema);
        this.MoverGemaFueraDelMapa(unaGema);
    }

    public void TirarGemas()
    {

        var nuevaPosicion = this.gameObject.transform.position;
        var nuevaAltura = 2.5f;
        

        foreach (GameObject unaGema in this.Gemas)
        {
            
            unaGema.GetComponentInChildren<Collider>().enabled = true;
            unaGema.GetComponentInChildren<Rigidbody>().useGravity = true;
            unaGema.GetComponentInChildren<Rigidbody>().velocity = Vector3.zero;
            nuevaPosicion.y += nuevaAltura;
            unaGema.transform.position = nuevaPosicion;
            nuevaAltura += 0.2f;
        }

        this.Gemas = new ArrayList();
    }

    public int CantidadGemas()
    {
        return this.Gemas.Count;
    }

    private void MoverGemaFueraDelMapa(GameObject unaGema)
    {
        unaGema.transform.position = new Vector3(100, 100, 100);
        unaGema.GetComponentInChildren<Rigidbody>().useGravity = false;
        unaGema.GetComponentInChildren<Collider>().enabled = false;
    }

    #endregion
}
