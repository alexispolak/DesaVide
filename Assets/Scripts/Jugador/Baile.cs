﻿using UnityEngine;
using System.Collections;
using System;

public class Baile : MonoBehaviour
{

    #region ATRIBUTOS
    public bool tieneQueBailar;
    public int animacionBaile;
    private Animator animador;
    #endregion

    #region PROPIEDADES
    #endregion

    #region METODOS UNITY
    // Use this for initialization
    void Start ()
    {
        this.animador = this.GetComponent<Animator>();

    }
	
	// Update is called once per frame
	void Update ()
    {
        if (this.tieneQueBailar)
            this.Bailar();
	}


    #endregion

    #region METODOS
    private void Bailar()
    {
        this.animador.SetLayerWeight(this.animador.GetLayerIndex("Bailes"),1f);
        this.animador.SetInteger("baile", animacionBaile);
        this.tieneQueBailar = false;
    }
    #endregion

}
