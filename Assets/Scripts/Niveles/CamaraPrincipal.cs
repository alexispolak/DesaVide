﻿using UnityEngine;
using System.Collections;
using System;
using UnityStandardAssets.ImageEffects;

public class CamaraPrincipal : MonoBehaviour
{
    #region ATRIBUTOS
    [SerializeField]
    private Transform focoCamara;
    #endregion

    #region PROPIEDADES
    private ArrayList Personajes { set; get; } 
    private bool ChequeoNuevosPersonajes { set; get; }
    private int CantidadPersonajesActual { set; get; }
    private Camera Camara { set; get; }
    private DepthOfField DepthOfField { set; get; }
    private int Iterador { set; get; }

    public Transform FocoCamara
    {
        get
        {
            return focoCamara;
        }

        set
        {
            focoCamara = value;
        }
    }
    #endregion

    #region METODOS UNITY

    // Use this for initialization
    void Start ()
    {
        this.Personajes = new ArrayList();
        this.Camara = this.GetComponent<Camera>();
        this.DepthOfField = this.GetComponent<DepthOfField>();

    }
	
	// Update is called once per frame
	void Update ()
    {
        if (this.ChequeoNuevosPersonajes)
            this.ObtenerPersonajes();

        if(this.CantidadPersonajesActual > 0)
            this.MoverPosicionSegunJugadores();
        

    }

    #endregion

    #region METODOS

    public void SeAgregoPersonaje()
    {
        this.ChequeoNuevosPersonajes = true;
    }

    public void SeEliminoPersonaje(GameObject personaje)
    {
        this.Personajes.Remove(personaje);
    }

    private void ObtenerPersonajes()
    {
        GameObject[] jugadores = GameObject.FindGameObjectsWithTag("jugador");
        this.Personajes = new ArrayList();

        for (this.Iterador = 0; this.Iterador < jugadores.Length; this.Iterador++)
        {
            this.Personajes.Add(jugadores[this.Iterador]);
        }

        this.CantidadPersonajesActual = this.Personajes.Count;
        this.ChequeoNuevosPersonajes = false;
    }

    private void MoverPosicionSegunJugadores()
    {
        if(this.Personajes != null)
        {
            var centroGravedad = this.CalcularCentroGravedad();

            this.MoverFoco(centroGravedad);

            this.transform.LookAt(centroGravedad);
            this.Camara.fieldOfView = 20+this.CalcularDistanciaEntreJugadores();//+ 25

            if (this.Camara.fieldOfView > 80)
                this.Camara.fieldOfView = 80;
        }
    }

    private void MoverFoco(Vector3 centroGravedad)
    {
        this.FocoCamara.position = centroGravedad;
        this.FocoCamara.LookAt(this.gameObject.transform.position);

        if (this.Camara.fieldOfView > 30)
            this.DepthOfField.focalSize = this.Map(this.Camara.fieldOfView, 45, 65, 0.14f, 0.65f);
        else
            this.DepthOfField.focalSize = 0.0f;
    }

    private Vector3 CalcularCentroGravedad()
    {
        float x = 0;
        float z = 0;
        var y = 0;
  
        for (this.Iterador = 0; this.Iterador < this.Personajes.Count; this.Iterador++)
        {
            x += ((GameObject)this.Personajes[this.Iterador]).transform.position.x;
            z += ((GameObject)this.Personajes[this.Iterador]).transform.position.z;
        }

        x /= this.Personajes.Count;
        z = (z/this.Personajes.Count);

        return new Vector3(x,y,z);
    }

    private float CalcularDistanciaEntreJugadores()
    {
        float distanciaMaxima = 0;

        for (int i= 0; i < this.Personajes.Count; i++)
        {
            for (this.Iterador = 0; this.Iterador < this.Personajes.Count; this.Iterador++)
            {
                if (i != this.Iterador)
                {
                    var distanciaEntreJugadores = Vector3.Distance(((GameObject)this.Personajes[i]).transform.position, ((GameObject)this.Personajes[this.Iterador]).transform.position);
                    if (distanciaEntreJugadores > distanciaMaxima)
                        distanciaMaxima = distanciaEntreJugadores;
                }
            }
        }

        return distanciaMaxima;
    }

    //Chequear bien este metodo
    public float Map(float value, float fromSource, float toSource, float fromTarget, float toTarget)
    {
        return (value - fromSource) / (toSource - fromSource) * (toTarget - fromTarget) + fromTarget;

    }
    #endregion
}
