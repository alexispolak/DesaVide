﻿using UnityEngine;
using System.Collections;
using System;

public class GiroGema : MonoBehaviour {

    public Vector3 EjeGiro;
    private Rigidbody RigidBody { set; get; }
    private Collider Collider { set; get; }

    // Update is called once per frame
    void Start()
    {
        this.RigidBody = this.GetComponent<Rigidbody>();
        this.Collider = this.GetComponent<Collider>();
    }

	void Update ()
    {
        this.Girar();
	}

    private void Girar()
    {
        this.transform.Rotate(EjeGiro);
    }

    public void MoverFueraDelMapa()
    {
        this.transform.position = new Vector3(100, 100, 100);
        this.RigidBody.useGravity = false;
        this.Collider.enabled = false;
    }
}
