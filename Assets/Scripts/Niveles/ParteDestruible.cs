﻿using UnityEngine;
using System.Collections;

public class ParteDestruible : MonoBehaviour
{
    #region ATRIBUTOS
    
    #endregion

    #region PROPIEDADES
    public Rigidbody RigidBody { private set; get; }
    public Collider Collider { private set; get; }

    #endregion

    #region METODOS UNITY
    // Use this for initialization
    void Awake ()
    {
        this.RigidBody = this.GetComponent<Rigidbody>();
        this.Collider = this.GetComponent<Collider>();   
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}
    #endregion

    #region METODOS
    #endregion
}
