﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Partida : MonoBehaviour {

    #region ATRIBUTOS
    [SerializeField]
    private GameObject[] jugador;
    [SerializeField]
    private bool esPartidaDePrueba;
    [SerializeField]
    private GameObject[] spawnPoints;
    [SerializeField]
    private GameObject[] listaSombreros;
  
    #endregion

    #region PROPIEDADES
    private List<GameObject> Jugadores { set; get; }
    private List<GameObject> JugadoresParticipando { set; get; }
    public float TemporizadorPartidaGanada { private set; get; }
    private bool TerminoLaPartida { set; get; }
    private ManejadorGui ManejadorGui { set; get; }
    public bool TengoQueActualizarBarraVida { set; get; }
    private GameObject CamaraPrincipal { set; get; }

    public GameObject[] SpawnPoints
    {
        get
        {
            return spawnPoints;
        }

        set
        {
            spawnPoints = value;
        }
    }

    public GameObject[] Jugador
    {
        get
        {
            return jugador;
        }

        set
        {
            jugador = value;
        }
    }

    public bool EsPartidaDePrueba
    {
        get
        {
            return esPartidaDePrueba;
        }

        set
        {
            esPartidaDePrueba = value;
        }
    }

    public GameObject[] ListaSombreros
    {
        get
        {
            return listaSombreros;
        }

        set
        {
            listaSombreros = value;
        }
    }
    #endregion

    #region METODOS UNITY

    // Use this for initialization
    void Start()
    {
        this.Jugadores = new List<GameObject>();
        this.JugadoresParticipando = new List<GameObject>();
        this.TemporizadorPartidaGanada = 5f;
        this.TerminoLaPartida = false;
        this.ManejadorGui = this.GetComponent<ManejadorGui>();
        this.CamaraPrincipal = GameObject.Find("Camera");
    }

    // Update is called once per frame
    void Update()
    {
        if (!this.EsPartidaDePrueba)
            this.ChequeoPartidaGanada();
        else if (this.ManejadorGui.SeVeElContador)
        { 
            this.ManejadorGui.OcultarTemporizador();
        }

        if (this.TengoQueActualizarBarraVida)
        {
            this.ActualizarBarrasVida();
        }
        
    }


    #endregion

    #region METODOS
    private void ActualizarBarrasVida()
    {
        for (int i = 0; i < this.Jugadores.Count; i++)
        {
            this.ManejadorGui.ActualizarGuiJugador(this.JugadoresParticipando[i].GetComponent<Jugador>().NumeroJugador, this.JugadoresParticipando[i].GetComponent<Jugador>().PorcentajeVida);
        }
    }

    public void Murio(GameObject jugador)
    {
        if (jugador.GetComponent<Jugador>().Vidas > 0)
            this.Respawn(jugador);
        else
            this.SacarDeLaPartida(jugador);
        
    }

    private void SacarDeLaPartida(GameObject jugador)
    {
        //Notifico a la camara.
        this.CamaraPrincipal.GetComponent<CamaraPrincipal>().SeEliminoPersonaje(jugador);

        //Lo saco de la partida.
        this.JugadoresParticipando.Remove(jugador);

        //Le resto una vida al jugador.
        jugador.GetComponent<Jugador>().Vidas -= 1;

        //Reproduzco la animacion de la Gui de la muerte.
        ((GameObject)this.ManejadorGui.GuiJugadores[jugador.GetComponent<Jugador>().NumeroJugador - 1]).GetComponent<GuiJugador>().ActualizarVidasJugador(jugador.GetComponent<Jugador>().Vidas);

        //Desactivo el gui del usuario ese.
        ((GameObject)this.ManejadorGui.GuiJugadores[jugador.GetComponent<Jugador>().NumeroJugador - 1]).GetComponent<GuiJugador>().DesactivarGui();

        //Lo Destruyo.
        Destroy(jugador);
    }

    private void ChequeoPartidaGanada()
    {
        if (this.JugadoresParticipando.Count == 1)
        {
            this.ManejadorGui.MostrarTemporizador();
            this.ContadorPartidaGanada(this.JugadoresParticipando[0]);
        }
        else if (this.JugadoresParticipando.Count == 0)
        {
            this.ManejadorGui.MostrarTemporizador();
            this.ContadorPartidaEmpatada();
        }
        else
            this.ManejadorGui.OcultarTemporizador();
    }

    private void ContadorPartidaGanada(GameObject jugadorGanador)
    {
        if (this.TemporizadorPartidaGanada > 0)
            this.TemporizadorPartidaGanada -= Time.deltaTime;
        else
            this.TerminarPartida(jugadorGanador);
    }

    private void ContadorPartidaEmpatada()
    {
        if (this.TemporizadorPartidaGanada > 0)
            this.TemporizadorPartidaGanada -= Time.deltaTime;
        else
            this.TerminarPartida();
    }

    private void TerminarPartida(GameObject jugador)
    {
        this.MostrarMensajeGanador(jugador);
        this.TerminoLaPartida = true;
    }

    

    private void TerminarPartida()
    {
        this.MostrarMensajeEmpate();
        this.TerminoLaPartida = true;
    }

    private void MostrarMensajeEmpate()
    {
        this.ManejadorGui.MostrarTerminarPartida();
    }

    private void MostrarMensajeGanador(GameObject jugador)
    {
        this.ManejadorGui.MostrarTerminarPartida(jugador);
    }

    public void IrAlMenuPrincipal()
    {
        SceneManager.LoadScene("Menu Principal");
    }

    private void Respawn(GameObject jugador)
    {
        
        int numeroJugador = jugador.GetComponent<Jugador>().NumeroJugador;

        //Le resto una vida al jugador
        jugador.GetComponent<Jugador>().Vidas -= 1;

        //Le reseteo el contador
        jugador.GetComponent<Jugador>().PorcentajeVida = 0;


        //Actualizo la interfaz
        ((GameObject)this.ManejadorGui.GuiJugadores[numeroJugador-1]).GetComponent<GuiJugador>().ActualizarVidasJugador(jugador.GetComponent<Jugador>().Vidas);
        this.ManejadorGui.ActualizarGuiJugador(jugador.GetComponent<Jugador>().NumeroJugador, jugador.GetComponent<Jugador>().PorcentajeVida);

        //Posiciono en su spawnpoint
        jugador.transform.position = this.SpawnPoints[numeroJugador - 1 ].transform.position;
        jugador.GetComponent<CuerpoJugador>().ControladorRagdoll.PartesDelCuerpo[0].transform.position = this.SpawnPoints[numeroJugador - 1].transform.position;


        //Activo al jugador
        jugador.GetComponent<CuerpoJugador>().Activarse();

    }

    public GameObject InstanciarNuevoJugador(int numeroJugador)
    {
        if(!this.TerminoLaPartida)
        {
            GameObject jugadorNuevo = this.Jugador[numeroJugador - 1];
            jugadorNuevo.SetActive(true);
            jugadorNuevo.transform.position = this.SpawnPoints[numeroJugador - 1].transform.position;
            jugadorNuevo.GetComponent<Jugador>().NumeroJugador = numeroJugador;
            jugadorNuevo.GetComponentInChildren<Sombrero>().Modelo = this.ListaSombreros[numeroJugador - 1];

            this.NotificarNuevoJugadorParaCamara();

            this.Jugadores.Add(jugadorNuevo);
            this.JugadoresParticipando.Add(jugadorNuevo);

            this.TemporizadorPartidaGanada = 5f;

            this.ManejadorGui.MostrarGuiJugador(numeroJugador, jugadorNuevo.GetComponent<Jugador>().PorcentajeVida);
            ((GameObject)this.ManejadorGui.GuiJugadores[numeroJugador - 1]).GetComponent<GuiJugador>().ActualizarVidasJugador(jugador[numeroJugador-1].GetComponent<Jugador>().Vidas);

            return jugadorNuevo;
        }

        return null;
    }

    private void NotificarNuevoJugadorParaCamara()
    {
        if (GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CamaraPrincipal>() != null)
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CamaraPrincipal>().SeAgregoPersonaje();
        else
        {
            Debug.Log("ENTRO NO DEBERIA ENTRAR ACÁ");
            //jugador.GetComponent<Rigidbody>().isKinematic = true;
        }
    }

    internal void ActualizarGemas(Jugador jugador)
    {
        ((GameObject)this.ManejadorGui.GuiJugadores[jugador.NumeroJugador - 1]).GetComponent<GuiJugador>().ActualizarGemas(jugador.CantidadGemas());
    }

    public void VolverAlMenuPrincipal()
    {
        SceneManager.LoadScene(0);
    }

    #endregion
}
