﻿using UnityEngine;
using System.Collections;
using System;

public class Explosion : MonoBehaviour
{
    #region ATRIBUTOS
    [SerializeField]
    private GameObject explosionParticulas;
    #endregion
    #region PROPIEDADES
    public GameObject ExplosionParticulas
    {
        get
        {
            return explosionParticulas;
        }

        set
        {
            explosionParticulas = value;
        }
    }
    private bool PuedoExplotar { set; get; }
    private float TemporizadorCollider { set; get; }
    private float TiempoDesactivarCollider { set; get; }
    private bool TengoQueDesactivarCollider { set; get; }
    #endregion

    // Update is called once per frame
    void Start()
    {
        this.PuedoExplotar = true;
        this.TengoQueDesactivarCollider = false;
        this.TiempoDesactivarCollider = .1f;
        this.TemporizadorCollider = this.TiempoDesactivarCollider;
    }

    void Update()
    {
        if(this.TengoQueDesactivarCollider)
            this.DesactivarCollider();
	}

    public void Explotar()
    {
        if(this.PuedoExplotar)
        { 
            this.ActivarParticulasExplosion();

            this.TengoQueDesactivarCollider = true;

            this.ReproducirSonido();

            this.PuedoExplotar = false;
        }
    }

    private void ReproducirSonido()
    {
        
    }

    private void ActivarParticulasExplosion()
    {
        GameObject nuevaExplocion = (GameObject)Instantiate(this.ExplosionParticulas, this.transform.position, Quaternion.Euler(Vector3.up));
        nuevaExplocion.GetComponent<ParticleSystem>().Play();
        Destroy(nuevaExplocion, 8f);
    }

    private void DesactivarCollider()
    {
        if (this.TemporizadorCollider > 0)
            this.TemporizadorCollider -= Time.deltaTime;
        else
        { 
            this.GetComponent<Collider>().enabled = false;
            this.TengoQueDesactivarCollider = false;
        }
    }
}
