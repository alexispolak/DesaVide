﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ManejadorGui : MonoBehaviour {

    #region ATRIBUTOS
    [SerializeField]
    private GameObject guiJugador1;
    [SerializeField]
    private GameObject guiJugador2;
    [SerializeField]
    private GameObject guiJugador3;
    [SerializeField]
    private GameObject guiJugador4;


    [SerializeField]
    private GameObject[] contador;
    [SerializeField]
    private GameObject menuTerminoPartida;
    [SerializeField]
    private GameObject mensajeGanador;
    [SerializeField]
    private GameObject imagenGanador;

    #endregion

    #region PROPIEDADES
    private Partida Partida { set; get; }
    public ArrayList GuiJugadores { private set; get; }
    public bool SeVeElContador { set; get; }

    public GameObject GuiJugador1
    {
        get
        {
            return guiJugador1;
        }

        set
        {
            guiJugador1 = value;
        }
    }

    public GameObject GuiJugador2
    {
        get
        {
            return guiJugador2;
        }

        set
        {
            guiJugador2 = value;
        }
    }

    public GameObject GuiJugador3
    {
        get
        {
            return guiJugador3;
        }

        set
        {
            guiJugador3 = value;
        }
    }

    public GameObject GuiJugador4
    {
        get
        {
            return guiJugador4;
        }

        set
        {
            guiJugador4 = value;
        }
    }

    public GameObject[] Contador
    {
        get
        {
            return contador;
        }

        set
        {
            contador = value;
        }
    }

    public GameObject MensajeGanador
    {
        get
        {
            return mensajeGanador;
        }

        set
        {
            mensajeGanador = value;
        }
    }

    public GameObject MenuTerminoPartida
    {
        get
        {
            return menuTerminoPartida;
        }

        set
        {
            menuTerminoPartida = value;
        }
    }

    public GameObject ImagenGanador
    {
        get
        {
            return imagenGanador;
        }

        set
        {
            imagenGanador = value;
        }
    }
    #endregion

    #region METODOS UNITY


    // Use this for initialization
    void Start()
    {
        this.Partida = this.GetComponent<Partida>();
        this.GuiJugadores = new ArrayList();
        this.GuiJugadores.Add(this.GuiJugador1);
        this.GuiJugadores.Add(this.GuiJugador2);
        this.GuiJugadores.Add(this.GuiJugador3);
        this.GuiJugadores.Add(this.GuiJugador4);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(this.SeVeElContador)
            this.ActualizarContador();
    }
    #endregion

    #region METODOS

    private void ActualizarContador()
    {
        this.Contador[0].GetComponent<Text>().text = this.Partida.TemporizadorPartidaGanada.ToString("0");
        this.Contador[1].GetComponent<Text>().text = this.Partida.TemporizadorPartidaGanada.ToString("0");
    }

    public void MostrarTemporizador()
    {
        this.SeVeElContador = true;
        this.Contador[0].GetComponent<Text>().enabled = true;
        this.Contador[1].GetComponent<Text>().enabled = true;

    }

    public void OcultarTemporizador()
    {
        this.SeVeElContador = false;
        this.Contador[0].GetComponent<Text>().enabled = false;
        this.Contador[1].GetComponent<Text>().enabled = false;
    }

    public void MostrarGuiJugador(int numeroJugador, float porcentaje)
    {
        ((GameObject)this.GuiJugadores[numeroJugador - 1]).SetActive(true);
        this.ActualizarGuiJugador(numeroJugador, porcentaje);
    }

    public void ActualizarGuiJugador(int numeroJugador, float porcentaje)
    {
        ((GameObject)this.GuiJugadores[numeroJugador - 1]).GetComponent<GuiJugador>().ActualizarBarraVidaJugador(porcentaje);
    }

    public void OcultarGuiJugador(int numeroJugador)
    {
        ((GameObject)this.GuiJugadores[numeroJugador]).SetActive(false);
    }

    public void MostrarTerminarPartida(GameObject jugador)
    {
        this.MenuTerminoPartida.SetActive(true);
        this.ImagenGanador.SetActive(true);
        this.OcultarTemporizador();
        this.MensajeGanador.GetComponent<Text>().text = "G A N A D O R\n\n\n Vidas: " + jugador.GetComponent<Jugador>().Vidas + " Gemas: " + jugador.GetComponent<Jugador>().CantidadGemas();
        this.imagenGanador.GetComponent<Image>().sprite = ((GameObject)this.GuiJugadores[jugador.GetComponent<Jugador>().NumeroJugador - 1]).GetComponent<GuiJugador>().ImagenJugador.GetComponent<Image>().sprite;

    }

    public void MostrarTerminarPartida()
    {
        this.MenuTerminoPartida.SetActive(true);
        this.OcultarTemporizador();
        this.MensajeGanador.GetComponent<Text>().text = "WTF ! ! ! \n La partida termino en empate...";
    }
    #endregion

}
