﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class GuiJugador : MonoBehaviour
{

    #region ATRIBUTOS
    [SerializeField]
    private GameObject imagenJugador;
    [SerializeField]
    private GameObject barraVidaJugador;
    [SerializeField]
    private GameObject cantidadVidasJugador;
    [SerializeField]
    private GameObject imagenJugadorMuerto;
    [SerializeField]
    private GameObject cantidadGemas;


    #endregion

    #region PROPIEDADES
    private Animator Animator { set; get; }
    private Slider BarraVida { set; get; }

    public GameObject ImagenJugador
    {
        get
        {
            return imagenJugador;
        }

        set
        {
            imagenJugador = value;
        }
    }

    public GameObject BarraVidaJugador
    {
        get
        {
            return barraVidaJugador;
        }

        set
        {
            barraVidaJugador = value;
        }
    }

    public GameObject CantidadVidasJugador
    {
        get
        {
            return cantidadVidasJugador;
        }

        set
        {
            cantidadVidasJugador = value;
        }
    }

    public GameObject ImagenJugadorMuerto
    {
        get
        {
            return imagenJugadorMuerto;
        }

        set
        {
            imagenJugadorMuerto = value;
        }
    }

    public GameObject CantidadGemas
    {
        get
        {
            return cantidadGemas;
        }

        set
        {
            cantidadGemas = value;
        }
    }

    #endregion

    #region METODOS UNITY


    // Use this for initialization
    void Awake()
    {
        this.Animator = this.GetComponent<Animator>();
        this.BarraVida = this.BarraVidaJugador.GetComponent<Slider>();
    }

    void Start()
    {  
        this.imagenJugadorMuerto.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #endregion

    #region METODOS

    public void ActualizarVidasJugador(int vidasJugador)
    {
        if (vidasJugador >= 0)
        { 
            this.RealizarAnimacionPerdidaVida();
            this.CantidadVidasJugador.GetComponent<Text>().text = vidasJugador.ToString();
        }
        else
        {
            this.BarraVida.value = 100;
            this.CantidadVidasJugador.SetActive(false);
            this.imagenJugadorMuerto.SetActive(true);
            this.Animator.Play("Muerte");
        }  
    }

    private void RealizarAnimacionPerdidaVida()
    {
        this.Animator.Play("PierdeUnaVida");
    }

    public void ActualizarGemas(int cantidadGemas)
    {
        this.CantidadGemas.GetComponent<Text>().text = cantidadGemas.ToString();
    }

    public void ActualizarBarraVidaJugador(float porcentaje)
    {
        this.BarraVida.value = porcentaje;        
    }

    internal void DesactivarGui()
    {
        this.gameObject.GetComponent<Animator>().enabled = false;
    }
    #endregion
}
