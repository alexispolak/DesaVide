﻿using UnityEngine;
using System.Collections;
using System;

public class ObjetoDestruible : MonoBehaviour
{
    #region ATRIBUTOS
    [SerializeField]
    private GameObject[] partes;
    [SerializeField]
    private bool esMeshCollider;
    [SerializeField]
    private float tiempoExplosion;
    public bool explosionManual;
    #endregion

    #region PROPIEDADES
    private float TemporizadorCollider { set; get; }
    private bool TengoQueActivarCollider { set; get; }
    private Rigidbody RigidBody { set; get; }
    private Explosion Explosion { set; get; }
    private BoxCollider[] ListaColliders { set; get; }
    private int Iterador { set; get; }
    private bool SeDestruyo { set; get; }


    public GameObject[] Partes
    {
        get
        {
            return partes;
        }

        set
        {
            partes = value;
        }
    }

    public bool EsMeshCollider
    {
        get
        {
            return esMeshCollider;
        }

        set
        {
            esMeshCollider = value;
        }
    }

    public float TiempoExplosion
    {
        get
        {
            return tiempoExplosion;
        }

        set
        {
            tiempoExplosion = value;
        }
    }
    #endregion

    #region METODOS UNITY

    // Use this for initialization
    void Start()
    {
        this.NoDestruible();
        this.TemporizadorCollider = 0f;
        this.TengoQueActivarCollider = false;   
        this.Explosion = this.GetComponent<Explosion>();
        this.SeDestruyo = false;
    }

    void Awake()
    {
        this.RigidBody = this.GetComponent<Rigidbody>();
        this.ListaColliders = this.GetComponents<BoxCollider>();
    }

    void Update()
    {
        if (this.explosionManual)
        {
            this.explosionManual = false;
            this.Destruible();
        }

        if (this.TengoQueActivarCollider)
        {
            if (this.TemporizadorCollider > 0)
                this.TemporizadorCollider -= Time.deltaTime;
            else if(!this.SeDestruyo)
                this.Destruir();
        }
    }
    #endregion

    #region METODOS

    private void Destruible()
    {
       
        this.TengoQueActivarCollider = true;

        if (this.TiempoExplosion.Equals(null))
            this.TiempoExplosion = 0f;

        this.TemporizadorCollider = this.TiempoExplosion;
    }

    private void Destruir()
    {
        this.SeDestruyo = true;

        this.CambiarEstado(false);

        if (this.Explosion != null)
            this.Explosion.Explotar();
    }

    private void NoDestruible()
    {
        this.CambiarEstado(true);
    }

    private void CambiarEstado (bool estadoNuevo)
    {
        if (!this.EsMeshCollider)
        {
            this.CambiarEstadoBoxCollider(estadoNuevo);
        }
        else
        {
            this.CambiarEstadoMeshCollider(estadoNuevo);
        }
    }

    private void CambiarEstadoMeshCollider(bool estadoNuevo)
    {
        ParteDestruible nuevaParte;
        for (this.Iterador = 0; this.Iterador < this.Partes.Length; this.Iterador++)
        {
            nuevaParte = this.Partes[this.Iterador].GetComponent<ParteDestruible>();
            nuevaParte.RigidBody.isKinematic = estadoNuevo;
            nuevaParte.Collider.enabled = !estadoNuevo;
        }

        this.RigidBody.isKinematic = !estadoNuevo;

        for (this.Iterador = 0; this.Iterador < this.ListaColliders.Length; this.Iterador++)
        {
            this.ListaColliders[this.Iterador].enabled = estadoNuevo;
        }
    }

    private void CambiarEstadoBoxCollider(bool estadoNuevo)
    {
        ParteDestruible nuevaParte;
        for (this.Iterador = 0; this.Iterador < this.Partes.Length; this.Iterador++)
        {
            nuevaParte = this.Partes[this.Iterador].GetComponent<ParteDestruible>();
            nuevaParte.RigidBody.isKinematic = estadoNuevo;
            nuevaParte.Collider.enabled = !estadoNuevo;
        }

        this.RigidBody.isKinematic = !estadoNuevo;


        for (this.Iterador = 0; this.Iterador < this.ListaColliders.Length; this.Iterador++)
        {
            this.ListaColliders[this.Iterador].enabled = estadoNuevo;
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "jugador" && col.gameObject.GetComponent<MovimientoJugador>().EstaAtacando())
        {
            this.Destruible();
        }
    }
    #endregion
}
