﻿using UnityEngine;
using System.Collections;
using System;

public class ManejadorControles : MonoBehaviour
{
    #region ATRIBUTOS
    
    #endregion

    #region PROPIEDADES
    private bool ExisteJugadorUno { set; get; }
    private bool ExisteJugadorDos { set; get; }
    private bool ExisteJugadorTres { set; get; }
    private bool ExisteJugadorCuatro { set; get; }
    #endregion

    #region METODOS UNITY
    #endregion
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        this.InicializarJugadores();
    }



    #region METODOS

    private void InicializarJugadores()
    {
        if (Input.GetButtonDown("Start1") && !this.ExisteJugadorUno)
        {
            this.ExisteJugadorUno = true;
            this.InicializarJugador(1);
        }
        if (Input.GetButtonDown("Start2") && !this.ExisteJugadorDos)
        {
            this.ExisteJugadorDos = true;
            this.InicializarJugador(2);
        }
        if (Input.GetButtonDown("Start3") && !this.ExisteJugadorTres)
        {
            this.ExisteJugadorTres = true;
            this.InicializarJugador(3);
        }
        if (Input.GetButtonDown("Start4") && !this.ExisteJugadorCuatro)
        {
            this.ExisteJugadorCuatro = true;
            this.InicializarJugador(4);
        }
    }

    public void InicializarJugador(int numeroJugador)
    {
        GameObject jugadorNuevo = this.GetComponent<Partida>().InstanciarNuevoJugador(numeroJugador);
        jugadorNuevo.GetComponent<MovimientoJugador>().J_Horizontal = "Horizontal" + numeroJugador;
        jugadorNuevo.GetComponent<MovimientoJugador>().J_Vertical = "Vertical" + numeroJugador;
        jugadorNuevo.GetComponent<MovimientoJugador>().J_Correr = "Correr" + numeroJugador;
        jugadorNuevo.GetComponent<MovimientoJugador>().J_Saltar = "Saltar" + numeroJugador;
        jugadorNuevo.GetComponent<MovimientoJugador>().J_Patada = "Patada" + numeroJugador;
        jugadorNuevo.GetComponent<MovimientoJugador>().J_Options = "Start" + numeroJugador;
        jugadorNuevo.GetComponent<MovimientoJugador>().J_Barrida = "Barrida" + numeroJugador;
        jugadorNuevo.GetComponent<MovimientoJugador>().NumeroJugador = numeroJugador;
    }

    public static void AsignarmeControl(GameObject jugadorNuevo, int numeroJugador)
    {
        jugadorNuevo.GetComponent<MovimientoJugador>().J_Horizontal = "Horizontal" + numeroJugador;
        jugadorNuevo.GetComponent<MovimientoJugador>().J_Vertical = "Vertical" + numeroJugador;
        jugadorNuevo.GetComponent<MovimientoJugador>().J_Correr = "Correr" + numeroJugador;
        jugadorNuevo.GetComponent<MovimientoJugador>().J_Saltar = "Saltar" + numeroJugador;
        jugadorNuevo.GetComponent<MovimientoJugador>().J_Patada = "Patada" + numeroJugador;
        jugadorNuevo.GetComponent<MovimientoJugador>().J_Options = "Start" + numeroJugador;
        jugadorNuevo.GetComponent<MovimientoJugador>().J_Barrida = "Barrida" + numeroJugador;
        jugadorNuevo.GetComponent<MovimientoJugador>().NumeroJugador = numeroJugador;
    }

    #endregion

}
