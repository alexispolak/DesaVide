﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{
    #region ATRIBUTOS
    [SerializeField]
    private GameObject objetoASpawnear;
    [SerializeField]
    private GameObject lugarSpawneo;
    [SerializeField]
    private float tiempoEntreSpawneo;
    [SerializeField]
    private float cantidadASpawnear;


    #endregion

    #region PROPIEDADES
    public GameObject ObjetoASpawnear
    {
        get
        {
            return objetoASpawnear;
        }

        set
        {
            objetoASpawnear = value;
        }
    }

    public GameObject LugarSpawneo
    {
        get
        {
            return lugarSpawneo;
        }

        set
        {
            lugarSpawneo = value;
        }
    }

    public float TiempoEntreSpawneo
    {
        get
        {
            return tiempoEntreSpawneo;
        }

        set
        {
            tiempoEntreSpawneo = value;
        }
    }

    public float CantidadASpawnear
    {
        get
        {
            return cantidadASpawnear;
        }

        set
        {
            cantidadASpawnear = value;
        }
    }
    #endregion


    #region METODOS UNITY


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider col)
    {
        this.SpawneraObjeto();
        Destroy(col.gameObject, 3f);
    }
    #endregion

    #region METODOS

    private void SpawneraObjeto()
    {
        GameObject nuevoSpawn = (GameObject) Instantiate(this.ObjetoASpawnear, this.LugarSpawneo.transform.position, Quaternion.identity);
    }


    #endregion

}
