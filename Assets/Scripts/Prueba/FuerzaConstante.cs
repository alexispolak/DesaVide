﻿using UnityEngine;
using System.Collections;

public class FuerzaConstante : MonoBehaviour
{

    #region ATRIBUTOS
    [SerializeField]private bool x;
    [SerializeField]private bool y;
    [SerializeField]private bool z;
    [SerializeField]private float fuerza;
    #endregion

    #region PROPIEDADES
    public bool X
    {
        get
        {
            return x;
        }

        set
        {
            x = value;
        }
    }

    public bool Y
    {
        get
        {
            return y;
        }

        set
        {
            y = value;
        }
    }
    public bool Z
    {
        get
        {
            return z;
        }

        set
        {
            z = value;
        }
    }

    public float Fuerza
    {
        get
        {
            return fuerza;
        }

        set
        {
            fuerza = value;
        }
    }
    #endregion

    // Update is called once per frame
    void Update ()
    {
        if (this.X)
        {
            this.GetComponent<Rigidbody>().AddForce(Vector3.up * this.Fuerza);
        }
        else
        {
            if (this.Y)
            {
                this.GetComponent<Rigidbody>().AddForce(Vector3.right * this.Fuerza);
            }
            else
            {
                this.GetComponent<Rigidbody>().AddForce(Vector3.left * this.Fuerza);
            }
        }
	}
}
