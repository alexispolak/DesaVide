﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MenuPrincipal : MonoBehaviour
{
    #region ATRIBUTOS
    [SerializeField]
    private EventSystem eventSystem;

#endregion

    #region PROPIEDADES
    public EventSystem EventSystem
    {
        get
        {
            return eventSystem;
        }

        set
        {
            eventSystem = value;
        }
    }

    private bool SeleccionoConTeclado { set; get; }
    private GameObject PrimerObjetoSeleccionado { set; get; }
    #endregion


    #region METODOS UNITY
    // Use this for initialization
    void Start()
    {
        this.PrimerObjetoSeleccionado = GameObject.Find("Comenzar");
    }

    // Update is called once per frame
    void Update()
    {
        this.MovimientoConTeclado();

    }
    #endregion

    #region METODOS
    private void MovimientoConTeclado()
    {
        if (Input.GetAxisRaw("Vertical1") != 0 && !this.SeleccionoConTeclado)
        {
            this.EventSystem.SetSelectedGameObject(this.PrimerObjetoSeleccionado);
            this.SeleccionoConTeclado = true;
        }
    }

    private void CuandoDesactivo()
    {
        this.SeleccionoConTeclado = false;
    }

    public void Salir()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public void Comenzar()
    {
        SceneManager.LoadScene("Nivel 1 low");
    }
#endregion


}
